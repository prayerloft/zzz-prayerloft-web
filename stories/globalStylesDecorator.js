import React from 'react';
import GlobalStyles from '../src/elements/GlobalStyles';

const globalStylesDecorator = story => <GlobalStyles>{story()}</GlobalStyles>;

export default globalStylesDecorator;

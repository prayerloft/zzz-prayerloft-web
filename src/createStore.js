import {
  createStore,
  applyMiddleware,
  compose as reduxCompose,
  combineReducers,
} from 'redux';
import thunk from 'redux-thunk';

import api from './api/callApi';
import appReducer from './ducks';
import createRouter from './createRouter';

const getCompose = env =>
  env === 'production'
    ? reduxCompose
    : window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || reduxCompose;

export default (env = 'development') => {
  const router = createRouter();
  const rootReducer = combineReducers({
    location: router.reducer,
    app: appReducer,
  });
  const compose = getCompose(env);

  const store = createStore(
    rootReducer,
    compose(
      router.enhancer,
      applyMiddleware(
        thunk.withExtraArgument(action =>
          api(action, store.dispatch, store.getState),
        ),
        router.middleware,
      ),
    ),
  );
  return store;
};

export const SHARE_PUBLIC = 'PUBLIC';
export const SHARE_ANONYMOUS = 'ANONYMOUS';
export const SHARE_PRIVATE = 'PRIVATE';

export const STATUS_WAITING = 'WAITING';
export const STATUS_ACTIVE = 'ACTIVE';
export const STATUS_EXPIRED = 'EXPIRED';
export const STATUS_DELETED = 'DELETED';

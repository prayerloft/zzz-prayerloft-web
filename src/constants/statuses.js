export const NOT_STARTED = 'NOT_STARTED';
export const PENDING = 'PENDING';
export const ERROR = 'ERROR';
export const SUCCESS = 'SUCCESS';

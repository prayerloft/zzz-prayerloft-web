export const CREATE_REQUEST = 'CREATE_REQUEST';
export const ACTIVE_REQUESTS = '/prayerRequests/active';
export const WAITING_REQUESTS = '/prayerRequests/waiting';

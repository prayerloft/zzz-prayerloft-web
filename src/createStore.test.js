import createStore from './createStore';

describe('#createStore', () => {
  test('it creates a store', () => {
    const store = createStore();
    expect(store).toHaveProperty('dispatch');
    expect(store).toHaveProperty('getState');
  });

  test('it creates a production store', () => {
    const store = createStore('production');
    expect(store).toHaveProperty('dispatch');
    expect(store).toHaveProperty('getState');
  });
});

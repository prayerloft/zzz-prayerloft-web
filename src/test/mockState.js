import { fromJS } from 'immutable';

const withViewShortName = () => ({
  location: {
    payload: {
      shortName: 'shortName',
    },
  },
  app: fromJS({
    view: {
      data: {
        shortName: 'shortName',
      },
    },
  }),
});

export { withViewShortName };

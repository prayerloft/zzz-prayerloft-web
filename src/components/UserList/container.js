import { connect } from 'react-redux';
import makeCreateStructuredSelector from '../../utils/makeCreateStructuredSelector';
import * as usersSagas from '../../sagas/users';
import * as usersSelectors from '../../selectors/users';

const mapActions = {
  onLoad: usersSagas.getAllUsers,
};

const makeMapState = makeCreateStructuredSelector({
  users: usersSelectors.makeAllUsersSelector,
});

export default connect(makeMapState, mapActions);

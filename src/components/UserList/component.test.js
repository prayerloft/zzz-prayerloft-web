import React from 'react';
import { shallow } from 'enzyme';

import UserList from './component';

describe('<UserList />', () => {
  test('it renders', () => {
    const wrapper = shallow(<UserList />);
    expect(wrapper.exists()).toBe(true);
  });
});

import React from 'react';
import PropTypes from 'prop-types';
import { List } from 'immutable';

import * as usersPolicies from '../../policies/users';

import Select from '../../elements/Select';
import lang from '../../lang';
import { noop } from '../../utils/defaults';

import UserListItem from '../UserListItem';

const userFilterItems = [
  { label: 'All Users', value: 'ALL' },
  { label: 'Admins', value: 'ADMINS' },
  { label: 'Moderators', value: 'MODERATORS' },
  { label: 'Basic Users', value: 'BASIC_USERS' },
];

class UserList extends React.PureComponent {
  static propTypes = {
    onLoad: PropTypes.func,
    users: PropTypes.instanceOf(List),
  };

  static defaultProps = {
    onLoad: noop,
    users: List(),
  };

  state = {
    filter: 'ALL',
  };

  componentDidMount() {
    this.props.onLoad();
  }

  onFilterChange = filter => {
    this.setState({ filter });
  };

  getFilteredUsers() {
    switch (this.state.filter) {
      case 'ALL':
        return this.props.users;
      case 'BASIC_USERS':
        return this.props.users.filter(usersPolicies.isBasicUser);
      case 'MODERATORS':
        return this.props.users.filter(usersPolicies.isOnlyModerator);
      case 'ADMINS':
        return this.props.users.filter(usersPolicies.isAdmin);
    }
  }

  renderUser = user => {
    return <UserListItem key={user.get('id')} user={user} />;
  };

  render() {
    return (
      <div>
        <Select
          value={this.state.filter}
          onChange={this.onFilterChange}
          label={lang.filter}
          options={userFilterItems}
        />
        {this.getFilteredUsers().map(this.renderUser)}
      </div>
    );
  }
}

export default UserList;

import React from 'react';
import { shallow } from 'enzyme';

import WaitingRequestList from './component';

describe('<WaitingRequestList />', () => {
  test('it renders', () => {
    const wrapper = shallow(<WaitingRequestList />);
    expect(wrapper.exists()).toBe(true);
  });
});

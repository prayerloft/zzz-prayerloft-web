import { connect } from 'react-redux';
import makeCreateStructuredSelector from '../../utils/makeCreateStructuredSelector';
import * as requestsSagas from '../../sagas/requests';
import * as entitiesSelectors from '../../selectors/entities';
import * as keys from '../../constants/keys';

const mapActions = {
  onFetchAll: requestsSagas.fetchWaitingRequests,
};

const makeMapState = makeCreateStructuredSelector({
  ids: entitiesSelectors.makeResultForKeySelector('/prayerRequests/waiting'),
  status: entitiesSelectors.makeStatusForKeySelector(keys.WAITING_REQUESTS),
});

export default connect(makeMapState, mapActions);

import React, { PureComponent } from 'react';
import { List } from 'immutable';
import PropTypes from 'prop-types';

import RequestListItem from '../RequestListItem';
import Loader from '../../elements/Loader';

class RequestList extends PureComponent {
  static propTypes = {
    pending: PropTypes.bool,
    blankSlate: PropTypes.node,
    ids: PropTypes.instanceOf(List),
    showEditControls: PropTypes.bool,
  };

  static defaultProps = {
    pending: false,
    blankSlate: null,
    ids: List(),
    showEditControls: false,
  };

  renderItems() {
    return (
      <div>
        {this.props.ids.map(id => (
          <RequestListItem
            showEditControls={this.props.showEditControls}
            key={id}
            id={id}
          />
        ))}
      </div>
    );
  }

  render() {
    if (this.props.pending) {
      return <Loader />;
    }
    if (this.props.ids.size) {
      return this.renderItems();
    }
    return this.props.blankSlate;
  }
}
export default RequestList;

import React from 'react';
import { shallow } from 'enzyme';
import { fromJS } from 'immutable';

import RequestList from './component';

describe('<RequestList />', () => {
  test('it renders', () => {
    const prayerList = shallow(<RequestList />);
    expect(prayerList.exists()).toBe(true);
  });

  test('it renders a prayer for each id', () => {
    const prayerList = shallow(<RequestList ids={fromJS([1, 2])} />);
    expect(prayerList.find('Connect(Jss(RequestListItem))').length).toBe(2);
  });
});

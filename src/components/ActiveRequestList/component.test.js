import React from 'react';
import { shallow } from 'enzyme';

import ActiveRequestList from './component';

describe('<ActiveRequestList />', () => {
  test('it renders', () => {
    const wrapper = shallow(<ActiveRequestList />);
    expect(wrapper.exists()).toBe(true);
  });
});

import { connect } from 'react-redux';
import makeCreateStructuredSelector from '../../utils/makeCreateStructuredSelector';
import * as requestsSagas from '../../sagas/requests';
import * as entitiesSelectors from '../../selectors/entities';
import * as keys from '../../constants/keys';

const mapActions = {
  onFetchAll: requestsSagas.fetchActiveRequests,
};

const makeMapState = makeCreateStructuredSelector({
  ids: entitiesSelectors.makeResultForKeySelector('/prayerRequests/active'),
  status: entitiesSelectors.makeStatusForKeySelector(keys.ACTIVE_REQUESTS),
});

export default connect(makeMapState, mapActions);

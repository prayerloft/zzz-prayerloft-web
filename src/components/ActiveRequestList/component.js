import React, { PureComponent } from 'react';
import { List } from 'immutable';
import PropTypes from 'prop-types';
import { noop } from '../../utils/defaults';

import RequestList from '../RequestList';
import BlankSlate from '../../elements/BlankSlate';

class ActiveRequestList extends PureComponent {
  static propTypes = {
    onFetchAll: PropTypes.func,
    ids: PropTypes.instanceOf(List),
    status: PropTypes.string,
  };

  static defaultProps = {
    onFetchAll: noop,
    ids: List(),
    status: 'NOT_STARTED',
  };

  componentWillMount() {
    this.props.onFetchAll();
  }

  renderBlankSlate() {
    return (
      <BlankSlate
        title="No prayer requests here!"
        subtitle="But you can submit one by clicking below."
      />
    );
  }

  render() {
    return (
      <RequestList
        ids={this.props.ids}
        blankSlate={this.renderBlankSlate()}
        pending={this.props.status === 'PENDING'}
      />
    );
  }
}
export default ActiveRequestList;

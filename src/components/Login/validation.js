import ValidatorHOC from '../../utils/ValidatorHOC';
import * as validations from '../../utils/validations';

export default ValidatorHOC({
  email: {
    isValid: validations.email,
    isRequired: true,
  },
  password: {
    isValid: validations.password,
    isRequired: true,
  },
});

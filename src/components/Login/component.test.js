import React from 'react';
import { shallow } from 'enzyme';

import Login from './component';

describe('<Login />', () => {
  test('it renders', () => {
    const wrapper = shallow(<Login />);
    expect(wrapper.exists()).toBe(true);
  });

  test('it shows email and password fields', () => {
    const wrapper = shallow(<Login />);
    expect(wrapper.find('[data-test="email"]').exists()).toBe(true);
    expect(wrapper.find('[data-test="password"]').exists()).toBe(true);
  });

  test('it calls onLogin with all fields', () => {
    const onLogin = jest.fn();
    const values = {
      email: 'kevindurb@gmail.com',
      password: 'somepassword',
    };

    const wrapper = shallow(<Login onLogin={onLogin} values={values} />);

    const login = wrapper.find('[data-test="login"]');

    login.simulate('click');

    const calledWithFirstArg = onLogin.mock.calls[0][0];

    expect(onLogin).toBeCalled();
    expect(calledWithFirstArg).toHaveProperty('email', 'kevindurb@gmail.com');
    expect(calledWithFirstArg).toHaveProperty('password', 'somepassword');
  });

  test('it doesnt call onLogin with any errors', () => {
    const onLogin = jest.fn();

    const wrapper = shallow(<Login onLogin={onLogin} hasErrors />);

    wrapper.find('[data-test="login"]').simulate('click');

    expect(onLogin).not.toBeCalled();
  });

  test('disables login button if validation error', () => {
    const wrapper = shallow(<Login hasErrors />);

    const login = wrapper.find('[data-test="login"]');

    expect(login.props()).toHaveProperty('isDisabled', true);
  });

  test('enables login button if no validation error', () => {
    const validation = {
      email: { isError: false },
      password: { isError: false },
    };

    const wrapper = shallow(<Login validation={validation} />);

    const login = wrapper.find('[data-test="login"]');

    expect(login.props()).toHaveProperty('isDisabled', false);
  });
});

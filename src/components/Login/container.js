import { connect } from 'react-redux';
import * as selectors from '../../selectors/users';
import * as sessionSagas from '../../sagas/session';
import * as viewActions from '../../ducks/view';
import * as locationSagas from '../../sagas/location';
import makeCreateStructuredSelector from '../../utils/makeCreateStructuredSelector';
import { makeEntitiesForKeySelector } from '../../selectors/entities';
import orgSchema from '../../schemas/org';

const mapActions = {
  onLogin: sessionSagas.login,
  onGoRegister() {
    return locationSagas.navigate(viewActions.REGISTER);
  },
};

const makeMapState = makeCreateStructuredSelector({
  org: makeEntitiesForKeySelector('/org', orgSchema),
  loginStatus: selectors.makeLoginStatusSelector,
  loginErrorCode: selectors.makeLoginErrorCodeSelector,
});

export default connect(makeMapState, mapActions);

import React from 'react';
import PropTypes from 'prop-types';
import { Map } from 'immutable';

import TextField from '../../elements/TextField';
import Button from '../../elements/Button';
import FormRow from '../../elements/FormRow';
import Header from '../../elements/Header';
import ValidatorField from '../../utils/ValidatorField';

import lang from '../../lang';
import { noop } from '../../utils/defaults';

const Email = ValidatorField('email')(TextField);
const Password = ValidatorField('password')(TextField);

class Login extends React.PureComponent {
  static propTypes = {
    org: PropTypes.object,
    onLogin: PropTypes.func,
    values: PropTypes.object,
    onGoRegister: PropTypes.func,
    setAllFieldsDirty: PropTypes.func,
    hasErrors: PropTypes.bool,
  };

  static defaultProps = {
    org: Map(),
    onLogin: noop,
    values: {},
    onGoRegister: noop,
    setAllFieldsDirty: noop,
    hasErrors: false,
  };

  onLogin = () => {
    const { email, password } = this.props.values;
    this.props.setAllFieldsDirty();

    if (!this.props.hasErrors) {
      this.props.onLogin({
        email,
        password,
      });
    }
  };

  render() {
    return (
      <div>
        <FormRow>
          <Header>{lang.login}</Header>
        </FormRow>
        <FormRow>
          <Email label={lang.email} type="email" data-test="email" />
        </FormRow>
        <FormRow>
          <Password
            label={lang.password}
            type="password"
            data-test="password"
          />
        </FormRow>
        <FormRow>
          <Button
            type="SECONDARY_DARK"
            data-test="register"
            onClick={this.props.onGoRegister}
          >
            {lang.register}
          </Button>
          <Button
            data-test="login"
            onClick={this.onLogin}
            isDisabled={this.props.hasErrors}
          >
            {lang.login}
          </Button>
        </FormRow>
      </div>
    );
  }
}

export default Login;

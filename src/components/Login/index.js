import { asyncComponent } from 'react-async-component';
import composeComponent from '../../utils/composeComponent';
import Loader from '../../elements/Loader';

export default asyncComponent({
  resolve: () =>
    Promise.all([
      System.import('./container'),
      System.import('./validation'),
      System.import('./component'),
    ]).then(composeComponent),
  name: 'Login',
  LoadingComponent: Loader,
});

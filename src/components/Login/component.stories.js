import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import globalStylesDecorator from '../../../stories/globalStylesDecorator';

import Login from './component';
import MainContent from '../../elements/MainContent';

storiesOf('Login', module)
  .addDecorator(globalStylesDecorator)
  .add('default', () => (
    <MainContent>
      <Login onLogin={action('onLogin')} />
    </MainContent>
  ));

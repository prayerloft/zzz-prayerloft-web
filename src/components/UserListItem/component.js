import React from 'react';
import PropTypes from 'prop-types';
import { Map } from 'immutable';

import * as usersPolicies from '../../policies/users';
import { noop } from '../../utils/defaults';

import ListItem from '../../elements/ListItem';
import Header from '../../elements/Header';
import Text from '../../elements/Text';
import Menu from '../../elements/Menu';
import Button from '../../elements/Button';
import Icon from '../../elements/Icon';
import Pill from '../../elements/Pill';

const basicUserItems = [
  { name: 'to Moderator', value: 'MODERATOR' },
  { name: 'to Admin', value: 'ADMIN' },
];

const moderatorItems = [
  { name: 'to Basic User', value: 'BASIC_USER' },
  { name: 'to Admin', value: 'ADMIN' },
];

const adminItems = [
  { name: 'to Basic User', value: 'BASIC_USER' },
  { name: 'to Moderator', value: 'MODERATOR' },
];

class UserListItem extends React.PureComponent {
  static propTypes = {
    user: PropTypes.instanceOf(Map),
    changeUserToRole: PropTypes.func,
    goToUser: PropTypes.func,
  };

  static defaultProps = {
    user: Map(),
    changeUserToRole: noop,
    goToUser: noop,
  };

  constructor(props) {
    super(props);

    this.state = {
      roleMenuOpen: false,
    };
  }

  goToUser = () => {
    this.props.goToUser(this.props.user.get('id'));
  };

  getRoleItems() {
    const user = this.props.user;
    if (usersPolicies.isBasicUser(user)) {
      return basicUserItems;
    } else if (usersPolicies.isOnlyModerator(user)) {
      return moderatorItems;
    }
    return adminItems;
  }

  getRole() {
    const user = this.props.user;
    if (user.get('isAdmin')) {
      return 'Admin';
    } else if (user.get('isModerator')) {
      return 'Moderator';
    }
    return 'User';
  }

  onClickRole = role => {
    this.props.changeUserToRole(this.props.user.get('id'), role);
  };

  renderRoleMenu() {
    return (
      <Menu xOffset={16} items={this.getRoleItems()} onClick={this.onClickRole}>
        <Button type="SECONDARY_DARK" noSidePadding>
          Change Role
          <Icon icon="arrow_drop_down" />
        </Button>
      </Menu>
    );
  }

  render() {
    return (
      <ListItem onClick={this.goToUser} rightContent={this.renderRoleMenu()}>
        <div>
          <Header size="h4">{this.props.user.get('name')}</Header>
          <Pill>{this.getRole()}</Pill>
        </div>
        <Text>{this.props.user.get('email')}</Text>
      </ListItem>
    );
  }
}

export default UserListItem;

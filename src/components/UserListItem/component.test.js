import React from 'react';
import { shallow } from 'enzyme';

import UserListItem from './component';

describe('<UserListItem />', () => {
  test('it renders', () => {
    const wrapper = shallow(<UserListItem />);
    expect(wrapper.exists()).toBe(true);
  });
});

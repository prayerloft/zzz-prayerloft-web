import { connect } from 'react-redux';
import makeCreateStructuredSelector from '../../utils/makeCreateStructuredSelector';
import * as usersSagas from '../../sagas/users';
import * as locationSagas from '../../sagas/location';
import * as viewActions from '../../ducks/view';

const mapActions = {
  changeUserToRole: usersSagas.changeUserToRole,
  goToUser(userId) {
    return locationSagas.navigate(viewActions.USER, { userId });
  },
};

const makeMapState = makeCreateStructuredSelector({});

export default connect(makeMapState, mapActions);

import React from 'react';
import { shallow } from 'enzyme';

import UserDetail from './component';

describe('<UserDetail />', () => {
  test('it renders', () => {
    const wrapper = shallow(<UserDetail />);
    expect(wrapper.exists()).toBe(true);
  });
});

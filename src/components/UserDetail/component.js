import React from 'react';
import PropTypes from 'prop-types';
import { Map } from 'immutable';
import { noop } from '../../utils/defaults';
import lang from '../../lang';

import FormRow from '../../elements/FormRow';
import TextField from '../../elements/TextField';
import Checkbox from '../../elements/Checkbox';
import Header from '../../elements/Header';
import Button from '../../elements/Button';
import Pill from '../../elements/Pill';
import ValidatorField from '../../utils/ValidatorField';
import * as usersPolicies from '../../policies/users';

const Email = ValidatorField('email')(TextField);
const Phone = ValidatorField('phone')(TextField);
const Notify = ValidatorField('notify', false)(Checkbox);

class UserDetail extends React.PureComponent {
  static propTypes = {
    user: PropTypes.instanceOf(Map),
    currentUser: PropTypes.instanceOf(Map),
    onLoad: PropTypes.func,
    userId: PropTypes.string,
    onSave: PropTypes.func,
    values: PropTypes.object,
  };

  static defaultProps = {
    user: Map(),
    currentUser: Map(),
    onLoad: noop,
    userId: '',
    onSave: noop,
    values: {},
  };

  constructor(props) {
    super(props);

    this.state = {
      isEditing: false,
    };
  }

  componentDidMount() {
    this.props.onLoad(this.props.userId);
  }

  onEdit = () => {
    this.setState({ isEditing: true });
  };

  onSave = () => {
    this.setState({ isEditing: false });
    this.props.onSave(this.props.userId, this.props.values);
  };

  getCanEdit() {
    const currentUser = this.props.currentUser;
    const user = this.props.user;
    return (
      currentUser.get('isAdmin') || currentUser.get('id') === user.get('id')
    );
  }

  getRole() {
    const user = this.props.user;
    if (user.get('isAdmin')) {
      return 'Admin';
    } else if (user.get('isModerator')) {
      return 'Moderator';
    }
    return 'User';
  }

  renderEditSave() {
    const isEditing = this.state.isEditing;
    const action = isEditing ? this.onSave : this.onEdit;
    const text = isEditing ? lang.save : lang.edit;

    return <Button onClick={action}>{text}</Button>;
  }

  renderNotifyMe() {
    const isEditing = this.state.isEditing;
    if (usersPolicies.atLeastModerator(this.props.user)) {
      return (
        <FormRow>
          <Notify label={lang.newPrayerNotify} isReadOnly={!isEditing} />
        </FormRow>
      );
    }
    return null;
  }

  render() {
    const isEditing = this.state.isEditing;

    return (
      <div>
        <FormRow justify="space-between">
          <FormRow>
            <Header>{this.props.user.get('name')}</Header>
            <Pill>{this.getRole()}</Pill>
          </FormRow>
          {this.renderEditSave()}
        </FormRow>
        <FormRow halfColumn>
          <Email
            label={lang.email}
            type="email"
            data-test="email"
            isReadOnly={!isEditing}
            autoFocus={isEditing}
          />
        </FormRow>
        <FormRow halfColumn>
          <Phone
            label={lang.phone}
            type="phone"
            data-test="phone"
            isReadOnly={!isEditing}
          />
        </FormRow>
        {this.renderNotifyMe()}
      </div>
    );
  }
}

export default UserDetail;

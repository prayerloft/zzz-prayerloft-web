import ValidatorHOC from '../../utils/ValidatorHOC';
import * as validations from '../../utils/validations';

export default ValidatorHOC({
  name: {
    isValid: validations.alwaysValid,
    isRequired: true,
  },
  email: {
    isValid: validations.alwaysValid,
    isRequired: false,
  },
  phone: {
    isValid: validations.alwaysValid,
    isRequired: false,
  },
  notify: {
    isValid: validations.alwaysValid,
    isRequired: false,
  },
});

import { connect } from 'react-redux';
import makeCreateStructuredSelector from '../../utils/makeCreateStructuredSelector';

import * as viewSelectors from '../../selectors/view';
import * as usersSelectors from '../../selectors/users';
import * as usersSagas from '../../sagas/users';

const mapActions = {
  onLoad: usersSagas.getUser,
  onSave: usersSagas.updateUser,
};

const makeMapState = makeCreateStructuredSelector({
  user: usersSelectors.makeUserFromPropsId,
  initialData: usersSelectors.makeUserFromPropsId,
  currentUser: usersSelectors.makeCurrentUserSelector,
  userId: viewSelectors.makeUserIdFromPayloadSelector,
});

export default connect(makeMapState, mapActions);

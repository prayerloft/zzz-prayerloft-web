import { connect } from 'react-redux';
import makeCreateStructuredSelector from '../../utils/makeCreateStructuredSelector';
import { makeEntitiesForKeySelector } from '../../selectors/entities';
import campusSchema from '../../schemas/campus';

const mapActions = {};

const makeMapState = makeCreateStructuredSelector({
  campuses: makeEntitiesForKeySelector('/campuses', [campusSchema]),
});

export default connect(makeMapState, mapActions);

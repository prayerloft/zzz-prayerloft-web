import React from 'react';
import PropTypes from 'prop-types';
import { List } from 'immutable';

import Select from '../../elements/Select';
import lang from '../../lang';
import { noop } from '../../utils/defaults';

class CampusSelect extends React.PureComponent {
  static propTypes = {
    campuses: PropTypes.instanceOf(List),
    onBlur: PropTypes.func,
    onChange: PropTypes.func,
    isError: PropTypes.bool,
    value: PropTypes.string,
  };

  static defaultProps = {
    campuses: [],
    onBlur: noop,
    onChange: noop,
    isError: false,
    value: '',
  };

  getCampusOptions() {
    return [
      { label: 'Choose...', value: '' },
      ...this.props.campuses.map(campus => ({
        label: campus.get('campusName'),
        value: campus.get('id'),
      })),
    ];
  }

  render() {
    if (this.props.campuses.size) {
      return (
        <Select
          label={lang.campuses}
          data-test="campuses"
          options={this.getCampusOptions()}
          onBlur={this.props.onBlur}
          onChange={this.props.onChange}
          isError={this.props.isError}
          value={this.props.value}
        />
      );
    }
    return null;
  }
}

export default CampusSelect;

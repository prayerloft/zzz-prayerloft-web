import React from 'react';
import { shallow } from 'enzyme';
import { fromJS } from 'immutable';

import TopNav from './component';

const emptyUser = fromJS({});

const createTopNav = props =>
  shallow(<TopNav currentUser={emptyUser} classes={{}} {...props} />);

describe('<TopNav />', () => {
  test('it renders', () => {
    const topNav = createTopNav();
    expect(topNav.exists()).toBe(true);
  });

  test('it should call onGoLogin when login clicked', () => {
    const onGoLogin = jest.fn();
    const topNav = createTopNav({ onGoLogin });
    topNav.find('[data-test="login"]').simulate('click');
    expect(onGoLogin).toBeCalled();
  });

  test('it should call onLogin when login clicked', () => {
    const onGoHome = jest.fn();
    const topNav = createTopNav({ onGoHome });
    topNav.find('[data-test="org-name"]').simulate('click');
    expect(onGoHome).toBeCalled();
  });

  test('it shows the username when logged in', () => {
    const currentUser = fromJS({
      id: '1234',
      name: 'bob',
    });
    const topNav = createTopNav({ currentUser });
    const userButton = topNav.find('[data-test="user"]');
    expect(userButton.props().children).toContain('bob');
  });
});

import injectSheet from 'react-jss';

export default injectSheet({
  orgName: {
    fontWeight: '500',
    cursor: 'pointer',
    fontSize: '18px',
    '@media (min-width: 480px)': {
      fontSize: '24px',
    },
  },
});

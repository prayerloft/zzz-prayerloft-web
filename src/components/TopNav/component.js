import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { Map } from 'immutable';

import * as usersPolicies from '../../policies/users';

import { noop } from '../../utils/defaults';
import lang from '../../lang';
import Button from '../../elements/Button';
import Icon from '../../elements/Icon';
import Menu from '../../elements/Menu';
import NavBar from '../../elements/NavBar';

const userMenuItems = [
  { name: 'My Profile', value: 'profile' },
  { name: 'Logout', value: 'logout' },
];

const adminMenuItems = [
  { name: 'My Profile', value: 'profile' },
  { name: 'Org Settings', value: 'org_settings' },
  { name: 'Logout', value: 'logout' },
];

class TopNav extends PureComponent {
  static propTypes = {
    org: PropTypes.object,
    onGoLogin: PropTypes.func,
    onGoProfile: PropTypes.func,
    onLogout: PropTypes.func,
    onGoHome: PropTypes.func,
    onGoOrgSettings: PropTypes.func,
    currentUser: PropTypes.object,
    classes: PropTypes.object,
  };

  static defaultProps = {
    org: Map(),
    onGoLogin: noop,
    onGoProfile: noop,
    onGoOrgSettings: noop,
    onLogout: noop,
    onGoHome: noop,
    currentUser: Map(),
  };

  onUserMenuClick = value => {
    switch (value) {
      case 'logout':
        this.props.onLogout();
        break;
      case 'profile':
        this.props.onGoProfile(this.props.currentUser.get('id'));
        break;
      case 'org_settings':
        this.props.onGoOrgSettings();
        break;
      default:
        break;
    }
  };

  isLoggedIn() {
    return !!this.props.currentUser.get('id');
  }

  isAdmin() {
    return usersPolicies.isAdmin(this.props.currentUser);
  }

  renderLoginLink() {
    return (
      <Button
        data-test="login"
        onClick={this.props.onGoLogin}
        type="SECONDARY_DARK"
      >
        {lang.loginsignup}
      </Button>
    );
  }

  renderUser() {
    const isAdmin = this.isAdmin();
    return (
      <Menu
        items={isAdmin ? adminMenuItems : userMenuItems}
        onClick={this.onUserMenuClick}
      >
        <Button data-test="user" type="SECONDARY_DARK" noSidePadding>
          {this.props.currentUser.get('name')}
          <Icon icon="arrow_drop_down" />
        </Button>
      </Menu>
    );
  }

  render() {
    return (
      <NavBar anchor="top" layout="space-between">
        <div
          data-test="org-name"
          onClick={this.props.onGoHome}
          className={this.props.classes.orgName}
        >
          {this.props.org.get('longName')}
        </div>
        {this.isLoggedIn() ? this.renderUser() : this.renderLoginLink()}
      </NavBar>
    );
  }
}

export default TopNav;

import component from './component';
import connect from './connect';
import styles from './styles';

export default connect(styles(component));

import { connect } from 'react-redux';
import makeCreateStructuredSelector from '../../utils/makeCreateStructuredSelector';

import * as usersSelectors from '../../selectors/users';
import * as viewActions from '../../ducks/view';
import * as sessionSagas from '../../sagas/session';
import * as locationSagas from '../../sagas/location';
import { makeEntitiesForKeySelector } from '../../selectors/entities';
import orgSchema from '../../schemas/org';

const mapActions = {
  onGoLogin() {
    return locationSagas.navigate(viewActions.LOGIN);
  },
  onGoProfile(id) {
    return locationSagas.navigate(viewActions.USER, { userId: id });
  },
  onGoHome() {
    return locationSagas.navigate(viewActions.PRAYER_LIST);
  },
  onGoOrgSettings() {
    return locationSagas.navigate(viewActions.ORG_SETTINGS);
  },
  onLogout: sessionSagas.logout,
};

const makeMapState = makeCreateStructuredSelector({
  org: makeEntitiesForKeySelector('/org', orgSchema),
  currentUser: usersSelectors.makeCurrentUserSelector,
});

export default connect(makeMapState, mapActions);

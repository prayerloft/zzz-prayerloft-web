import React from 'react';
import { shallow } from 'enzyme';

import NotificationCenter from './component';

describe('<NotificationCenter />', () => {
  test('it renders', () => {
    const wrapper = shallow(<NotificationCenter />);
    expect(wrapper.exists()).toBe(true);
  });
});

import { connect } from 'react-redux';
import makeCreateStructuredSelector from '../../utils/makeCreateStructuredSelector';

import * as notificationsSelectors from '../../selectors/notifications';

const mapActions = {};

const makeMapState = makeCreateStructuredSelector({
  notifications: notificationsSelectors.makeAllNotificationsSelector,
});

export default connect(makeMapState, mapActions);

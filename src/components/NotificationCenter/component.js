import React from 'react';
import PropTypes from 'prop-types';
import { List } from 'immutable';

import Notification from '../../elements/Notification';

class NotificationCenter extends React.PureComponent {
  static propTypes = {
    notifications: PropTypes.instanceOf(List),
  };

  static defaultProps = {
    notifications: List(),
  };

  constructor(props) {
    super(props);

    this.state = {
      now: Date.now(),
    };
  }

  componentDidMount() {
    this.interval = setInterval(this.updateNow, 1000);
  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }

  updateNow = () => {
    this.setState({ now: Date.now() });
  };

  getActiveNotifications() {
    const now = this.state.now;
    return this.props.notifications.filter(
      notification =>
        notification.get('createdAt') + notification.get('duration') > now,
    );
  }

  renderNotification = (notification, key) => (
    <Notification type={notification.get('type')} key={key}>
      {notification.get('text')}
    </Notification>
  );

  renderNotifications() {
    return this.getActiveNotifications()
      .map(this.renderNotification)
      .toArray();
  }

  render() {
    return <div>{this.renderNotifications()}</div>;
  }
}

export default NotificationCenter;

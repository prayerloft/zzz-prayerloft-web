import React from 'react';
import PropTypes from 'prop-types';
import { Map } from 'immutable';

import FormRow from '../../elements/FormRow';
import Header from '../../elements/Header';

import UserList from '../UserList';

class OrgSettings extends React.PureComponent {
  static propTypes = {
    org: PropTypes.instanceOf(Map),
  };

  static defaultProps = {
    org: Map(),
  };

  render() {
    return (
      <div>
        <FormRow justify="space-between">
          <Header>{this.props.org.get('longName')}</Header>
        </FormRow>
        <UserList />
      </div>
    );
  }
}

export default OrgSettings;

import React from 'react';
import { shallow } from 'enzyme';

import OrgSettings from './component';

describe('<OrgSettings />', () => {
  test('it renders', () => {
    const wrapper = shallow(<OrgSettings />);
    expect(wrapper.exists()).toBe(true);
  });
});

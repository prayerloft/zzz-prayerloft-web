import { connect } from 'react-redux';
import makeCreateStructuredSelector from '../../utils/makeCreateStructuredSelector';

import { makeEntitiesForKeySelector } from '../../selectors/entities';
import orgSchema from '../../schemas/org';

const mapActions = {};

const makeMapState = makeCreateStructuredSelector({
  org: makeEntitiesForKeySelector('/org', orgSchema),
});

export default connect(makeMapState, mapActions);

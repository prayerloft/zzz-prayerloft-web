import * as React from 'react';
import { noop } from '../../utils/defaults';

import TopNav from '../TopNav';
import BottomNav from '../BottomNav';
import ActiveRequestList from '../ActiveRequestList';
import WaitingRequestList from '../WaitingRequestList';
import CreateRequest from '../CreateRequest';
import UserDetail from '../UserDetail';
import OrgSettings from '../OrgSettings';
import MainContent from '../../elements/MainContent';
import NotificationCenter from '../NotificationCenter';
import Register from '../Register';
import Login from '../Login';

import * as viewConstants from '../../ducks/view';

interface Props {
  shortName: string;
  onLoad: Function;
  view: string;
  classes: {
    app: string;
  };
}

export default class App extends React.PureComponent<Props, {}> {
  static defaultProps = {
    view: '',
    onLoad: noop,
    shortName: '',
  };

  componentWillMount() {
    if (this.props.shortName) {
      this.props.onLoad(this.props.shortName);
    }
  }

  renderMainContent() {
    switch (this.props.view) {
      case viewConstants.PRAYER_LIST:
        return <ActiveRequestList />;
      case viewConstants.PRAYER_REQUEST:
        return <CreateRequest />;
      case viewConstants.REGISTER:
        return <Register />;
      case viewConstants.LOGIN:
        return <Login />;
      case viewConstants.MODERATE:
        return <WaitingRequestList />;
      case viewConstants.USER:
        return <UserDetail />;
      case viewConstants.ORG_SETTINGS:
        return <OrgSettings />;
      default:
        return null;
    }
  }

  render() {
    const classes = this.props.classes || {};
    if (this.props.shortName) {
      return (
        <div className={classes.app}>
          <MainContent>
            <NotificationCenter />
            {this.renderMainContent()}
          </MainContent>
          <TopNav />
          <BottomNav />
        </div>
      );
    }
    return null;
  }
}

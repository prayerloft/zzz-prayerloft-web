import { connect } from 'react-redux';
import makeCreateStructuredSelector from '../../utils/makeCreateStructuredSelector';

import * as appSagas from '../../sagas/app';
import * as viewSelectors from '../../selectors/view';
import { makeEntitiesForKeySelector } from '../../selectors/entities';
import orgSchema from '../../schemas/org';

const mapActions = {
  onLoad: appSagas.startApp,
};

const makeMapState = makeCreateStructuredSelector({
  org: makeEntitiesForKeySelector('/org', orgSchema),
  shortName: viewSelectors.makeShortNameFromPayloadSelector,
  view: viewSelectors.makeCurrentViewSelector,
});

export default connect(makeMapState, mapActions);

import injectSheet from 'react-jss';

export default injectSheet({
  app: {
    maxWidth: '1024px',
    flexBasis: '100%',
    display: 'flex',
    justifyContent: 'center',
  },
});

import React from 'react';
import { shallow } from 'enzyme';
import sinon from 'sinon';

import App from './component';
import * as viewConstants from '../../ducks/view';

describe('<App />', () => {
  test('renders TopNav always', () => {
    const app = shallow(<App shortName="shortName" />);
    expect(app.find('Connect(Jss(TopNav))').exists()).toBe(true);
  });

  test('renders prayer list with view prop', () => {
    const app = shallow(
      <App view={viewConstants.PRAYER_LIST} shortName="shortName" />,
    );
    expect(app.find('ActiveRequestList').exists()).toBe(true);
  });

  test('renders create prayer request with view prop', () => {
    const app = shallow(
      <App view={viewConstants.PRAYER_REQUEST} shortName="shortName" />,
    );
    expect(app.find('CreateRequest').exists()).toBe(true);
  });

  test('renders register with view prop', () => {
    const app = shallow(
      <App view={viewConstants.REGISTER} shortName="shortName" />,
    );
    expect(app.find('Register').exists()).toBe(true);
  });

  test('renders login with view prop', () => {
    const app = shallow(
      <App view={viewConstants.LOGIN} shortName="shortName" />,
    );
    expect(app.find('Login').exists()).toBe(true);
  });

  test('it calls onLoad after mount', () => {
    const onLoad = sinon.spy();
    shallow(<App onLoad={onLoad} shortName="shortName" />);
    expect(onLoad.calledOnce).toBe(true);
  });
});

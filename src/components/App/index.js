import { asyncComponent } from 'react-async-component';
import composeComponent from '../../utils/composeComponent';
import Loader from '../../elements/Loader';

export default asyncComponent({
  resolve: () =>
    Promise.all([
      System.import('./container'),
      System.import('./styles'),
      System.import('./component'),
    ]).then(composeComponent),
  name: 'App',
  LoadingComponent: Loader,
});

import connect from './connect';
import styles from './styles';
import component from './component';

export default connect(styles(component));

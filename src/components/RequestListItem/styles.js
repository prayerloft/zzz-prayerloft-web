import injectSheet from 'react-jss';
import colors from '../../colors';

export default injectSheet({
  '@keyframes pendingFadeIn': {
    from: { opacity: 0 },
    to: { opacity: 1 },
  },
  '@keyframes scaleToZero': {
    from: {
      transform: 'scale(1, 1)',
      opacity: '1',
    },
    to: {
      transform: 'scale(1, 0)',
      opacity: '0',
    },
  },
  pending: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    backgroundColor: colors.secondary,
    margin: '0 -16px',
    opacity: '0',
    animation: '250ms pendingFadeIn',
    animationFillMode: 'forwards',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    color: colors.white,
  },
  close: {
    animation: '500ms scaleToZero cubic-bezier(0.165, 0.84, 0.44, 1)',
    animationFillMode: 'forwards',
    animationDelay: '1.5s',
  },
});

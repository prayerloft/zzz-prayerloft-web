import { connect } from 'react-redux';
import makeCreateStructuredSelector from '../../utils/makeCreateStructuredSelector';

import * as prayersSelectors from '../../selectors/prayers';
import * as prayersSagas from '../../sagas/prayers';
import * as requestsSagas from '../../sagas/requests';
import { makeEntityForPropsIdSelector } from '../../selectors/entities';
import prayerRequestSchema from '../../schemas/prayerRequest';

const mapActions = {
  onPray: prayersSagas.prayForRequest,
  onAccept: requestsSagas.acceptRequest,
  onDelete: requestsSagas.deleteRequest,
};

const makeMapState = makeCreateStructuredSelector({
  request: makeEntityForPropsIdSelector(prayerRequestSchema),
  isPending: prayersSelectors.makePropsIdIsPendingSelector,
});

export default connect(makeMapState, mapActions);

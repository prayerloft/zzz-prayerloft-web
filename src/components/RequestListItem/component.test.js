import React from 'react';
import sinon from 'sinon';
import { shallow } from 'enzyme';

import Request from './component';

const createRequest = props => shallow(<Request classes={{}} {...props} />);

describe('<Request />', () => {
  test('it renders', () => {
    const prayer = createRequest();
    expect(prayer.exists()).toBe(true);
  });

  xtest('it calls onSelect with id when pray button is clicked', () => {
    const onPray = sinon.spy();
    const prayer = createRequest({
      id: 'theid',
      onPray,
    });
    prayer.find('[data-test="pray"]').simulate('click');
    expect(onPray.calledWith('theid')).toBe(true);
  });

  xtest('pray button is shown', () => {
    const prayer = createRequest({
      id: 'theid',
    });
    expect(prayer.find('[data-test="pray"]').exists()).toBe(true);
  });
});

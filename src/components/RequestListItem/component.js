import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { Map } from 'immutable';
import classnames from 'classnames';
import { noop } from '../../utils/defaults';

import Button from '../../elements/Button';
import ListItem from '../../elements/ListItem';
import Header from '../../elements/Header';
import Text from '../../elements/Text';

class RequestListItem extends PureComponent {
  static propTypes = {
    id: PropTypes.string,
    request: PropTypes.object,
    onPray: PropTypes.func,
    onAccept: PropTypes.func,
    onDelete: PropTypes.func,
    classes: PropTypes.object,
    isPending: PropTypes.bool,
    showEditControls: PropTypes.bool,
  };

  static defaultProps = {
    id: '',
    request: Map(),
    onPray: noop,
    onAccept: noop,
    onDelete: noop,
    isPending: false,
    showEditControls: false,
  };

  onPray = () => {
    this.props.onPray(this.props.id);
  };

  onAccept = () => {
    this.props.onAccept(this.props.id);
  };

  onDelete = () => {
    this.props.onDelete(this.props.id);
  };

  renderPrayButton() {
    return (
      <Button data-test="pray" onClick={this.onPray}>
        Pray
      </Button>
    );
  }

  renderPending() {
    if (this.props.isPending) {
      return (
        <div className={this.props.classes.pending}>Thank you for praying!</div>
      );
    }

    return null;
  }

  renderModerateControls() {
    return [
      <Button key="accept" onClick={this.onAccept}>
        Accept
      </Button>,
      <Button key="delete" type="SECONDARY_DARK" onClick={this.onDelete}>
        Delete
      </Button>,
    ];
  }

  renderControls() {
    if (this.props.showEditControls) {
      return this.renderModerateControls();
    }
    return this.renderPrayButton();
  }

  render() {
    const request = this.props.request;
    const classes = this.props.classes || {};

    const rootClasses = classnames({
      [classes.close]: this.props.isPending,
    });

    return (
      <div className={rootClasses}>
        <ListItem rightContent={this.renderControls()}>
          <Header size="h4">{request.get('name')}</Header>
          <Text>{request.get('prayer')}</Text>
          {this.renderPending()}
        </ListItem>
      </div>
    );
  }
}

export default RequestListItem;

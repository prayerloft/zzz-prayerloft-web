import { connect } from 'react-redux';
import makeCreateStructuredSelector from '../../utils/makeCreateStructuredSelector';

import * as viewActions from '../../ducks/view';
import * as usersSelectors from '../../selectors/users';
import * as locationSagas from '../../sagas/location';
import * as viewSelectors from '../../selectors/view';

const mapActions = {
  onGoRequests() {
    return locationSagas.navigate(viewActions.PRAYER_LIST);
  },
  onGoCreateRequest() {
    return locationSagas.navigate(viewActions.PRAYER_REQUEST);
  },
  onGoModerate() {
    return locationSagas.navigate(viewActions.MODERATE);
  },
};

const makeMapState = makeCreateStructuredSelector({
  currentUser: usersSelectors.makeCurrentUserSelector,
  view: viewSelectors.makeCurrentViewSelector,
});

export default connect(makeMapState, mapActions);

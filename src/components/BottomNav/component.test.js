import React from 'react';
import { shallow } from 'enzyme';

import BottomNav from './component';

const createBottomNav = props => shallow(<BottomNav classes={{}} {...props} />);

describe('<BottomNav />', () => {
  test('it renders', () => {
    const bottomNav = createBottomNav();
    expect(bottomNav.exists()).toBe(true);
  });
});

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { Map } from 'immutable';

import { atLeastModerator } from '../../policies/users';

import { noop } from '../../utils/defaults';
import Button from '../../elements/Button';
import Icon from '../../elements/Icon';
import NavBar from '../../elements/NavBar';

import * as viewConstants from '../../ducks/view';

export default class BottomNav extends PureComponent {
  static propTypes = {
    onGoRequests: PropTypes.func,
    onGoCreateRequest: PropTypes.func,
    onGoModerate: PropTypes.func,
    currentUser: PropTypes.instanceOf(Map),
    view: PropTypes.string,
  };

  static defaultProps = {
    onGoRequests: noop,
    onGoCreateRequest: noop,
    onGoModerate: noop,
    currentUser: Map(),
    view: '',
  };

  renderIcon(icon, action, selected) {
    return (
      <Button onClick={action} type="SECONDARY_DARK">
        <Icon big selected={selected} icon={icon} />
      </Button>
    );
  }

  renderRequests() {
    const selected = this.props.view === viewConstants.PRAYER_LIST;
    return this.renderIcon('list', this.props.onGoRequests, selected);
  }

  renderCreateRequest() {
    const selected = this.props.view === viewConstants.PRAYER_REQUEST;
    return this.renderIcon('add', this.props.onGoCreateRequest, selected);
  }

  renderModerate() {
    if (atLeastModerator(this.props.currentUser)) {
      const selected = this.props.view === viewConstants.MODERATE;
      return this.renderIcon('edit', this.props.onGoModerate, selected);
    }
    return null;
  }

  render() {
    return (
      <NavBar anchor="bottom" layout="space-around">
        {this.renderRequests()}
        {this.renderCreateRequest()}
        {this.renderModerate()}
      </NavBar>
    );
  }
}

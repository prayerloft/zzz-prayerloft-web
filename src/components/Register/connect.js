import { connect } from 'react-redux';
import makeCreateStructuredSelector from '../../utils/makeCreateStructuredSelector';
import * as selectors from '../../selectors/users';
import * as usersSagas from '../../sagas/users';
import * as viewActions from '../../ducks/view';
import { makeEntitiesForKeySelector } from '../../selectors/entities';
import orgSchema from '../../schemas/org';

const mapActions = {
  onRegister: usersSagas.register,
  onGoLogin: viewActions.login,
};

const makeMapState = makeCreateStructuredSelector({
  org: makeEntitiesForKeySelector('/org', orgSchema),
  registerStatus: selectors.makeRegisterStatusSelector,
});

export default connect(makeMapState, mapActions);

import ValidatorHOC from '../../utils/ValidatorHOC';
import * as validations from '../../utils/validations';

export default ValidatorHOC({
  name: {
    isValid: validations.alwaysValid,
    isRequired: true,
  },
  email: {
    isValid: validations.email,
    isRequired: true,
  },
  password: {
    isValid: validations.password,
    isRequired: true,
  },
});

import React from 'react';
import sinon from 'sinon';
import { shallow } from 'enzyme';
import * as statuses from '../../constants/statuses';

import Register from './component';

describe('<Register />', () => {
  test('it renders', () => {
    const wrapper = shallow(<Register />);
    expect(wrapper.exists()).toBe(true);
  });

  test('it calles onRegister with all fields', () => {
    const onRegister = sinon.spy();
    const values = {
      name: 'kevin',
      email: 'kevindurb@gmail.com',
      password: 'somepassword',
    };

    const wrapper = shallow(
      <Register onRegister={onRegister} values={values} />,
    );

    const register = wrapper.find('[data-test="register"]');

    register.simulate('click');

    const calledWithFirstArg = onRegister.getCall(0).args[0];

    expect(onRegister.calledOnce).toBe(true);

    expect(calledWithFirstArg).toHaveProperty('name', 'kevin');
    expect(calledWithFirstArg).toHaveProperty('email', 'kevindurb@gmail.com');
    expect(calledWithFirstArg).toHaveProperty('password', 'somepassword');
  });

  test('it doesnt call onRegister with any errors', () => {
    const onRegister = jest.fn();
    const validation = {
      email: { isError: false },
      password: { isError: true },
    };

    const wrapper = shallow(
      <Register onRegister={onRegister} validation={validation} />,
    );

    wrapper.find('[data-test="register"]').simulate('click');

    expect(onRegister).not.toBeCalled();
  });

  test('disables register button if validation error', () => {
    const validation = {
      name: { isError: true },
      email: { isError: false },
      password: { isError: true },
    };

    const wrapper = shallow(<Register validation={validation} />);

    const register = wrapper.find('[data-test="register"]');

    expect(register.props()).toHaveProperty('isDisabled', true);
  });

  test('enables register button if no validation error', () => {
    const validation = {
      name: { isError: false },
      email: { isError: false },
      password: { isError: false },
    };

    const wrapper = shallow(<Register validation={validation} />);

    const register = wrapper.find('[data-test="register"]');

    expect(register.props()).toHaveProperty('isDisabled', false);
  });

  test('calls onFieldDirty when a field blurs', () => {
    const onFieldDirty = sinon.spy();
    const wrapper = shallow(<Register onFieldDirty={onFieldDirty} />);

    const name = wrapper.find('[data-test="name"]');
    const email = wrapper.find('[data-test="email"]');
    const password = wrapper.find('[data-test="password"]');

    name.simulate('blur');
    email.simulate('blur');
    password.simulate('blur');

    expect(onFieldDirty.calledWith('name')).toBe(true);
    expect(onFieldDirty.calledWith('email')).toBe(true);
    expect(onFieldDirty.calledWith('password')).toBe(true);
  });

  test('calls onChange when a field changes', () => {
    const onChange = sinon.spy();
    const wrapper = shallow(<Register onChange={onChange} />);

    const name = wrapper.find('[data-test="name"]');
    const email = wrapper.find('[data-test="email"]');
    const password = wrapper.find('[data-test="password"]');

    name.simulate('change', 'kevin');
    email.simulate('change', 'kevindurb@gmail.com');
    password.simulate('change', 'somepassword');

    expect(onChange.calledWith('name', 'kevin')).toBe(true);
    expect(onChange.calledWith('email', 'kevindurb@gmail.com')).toBe(true);
    expect(onChange.calledWith('password', 'somepassword')).toBe(true);
  });

  test('renders success when success status', () => {
    const wrapper = shallow(<Register registerStatus={statuses.SUCCESS} />);

    expect(wrapper.find('SuccessPage').exists()).toBe(true);
  });
});

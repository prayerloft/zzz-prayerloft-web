// TODO: refactor with new validator field
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { Map } from 'immutable';

import TextField from '../../elements/TextField';
import Button from '../../elements/Button';
import FormRow from '../../elements/FormRow';
import SuccessPage from '../SuccessPage';
import Header from '../../elements/Header';

import { noop } from '../../utils/defaults';
import lang from '../../lang';

import * as statuses from '../../constants/statuses';

class Register extends PureComponent {
  static propTypes = {
    onRegister: PropTypes.func,
    onChange: PropTypes.func,
    onFieldDirty: PropTypes.func,
    validation: PropTypes.object,
    values: PropTypes.object,
    registerStatus: PropTypes.string,
    onGoLogin: PropTypes.func,
    org: PropTypes.instanceOf(Map),
  };

  static defaultProps = {
    onRegister: noop,
    onChange: noop,
    onFieldDirty: noop,
    validation: {},
    values: {},
    registerStatus: '',
    onGoLogin: noop,
    org: Map(),
  };

  onChangeName = name => this.props.onChange('name', name);
  onChangeEmail = email => this.props.onChange('email', email);
  onChangePassword = password => this.props.onChange('password', password);

  onBlurName = () => this.props.onFieldDirty('name');
  onBlurEmail = () => this.props.onFieldDirty('email');
  onBlurPassword = () => this.props.onFieldDirty('password');

  onRegister = () => {
    const { name, email, password } = this.props.values;

    const hasAnyErrors = this.getHasAnyErrors();

    if (!hasAnyErrors) {
      this.props.onRegister({
        name,
        email,
        password,
      });
    } else {
      this.onBlurName();
      this.onBlurEmail();
      this.onBlurPassword();
    }
  };

  onGoLogin = () => {
    this.props.onGoLogin({ shortName: this.props.org.get('shortName') });
  };

  getIsErrorForField = field =>
    this.props.validation[field] && this.props.validation[field].showError;

  getHasAnyErrors = () => {
    const validation = this.props.validation;
    const fieldNames = Object.keys(validation);
    return fieldNames.reduce(
      (hasError, fieldName) => hasError || validation[fieldName].isError,
      false,
    );
  };

  renderForm() {
    const values = this.props.values;
    const hasAnyErrors = this.getHasAnyErrors();
    return (
      <div>
        <FormRow>
          <Header>{lang.register}</Header>
        </FormRow>
        <FormRow>
          <TextField
            label={lang.name}
            data-test="name"
            onBlur={this.onBlurName}
            isError={this.getIsErrorForField('name')}
            value={values.name}
            onChange={this.onChangeName}
          />
        </FormRow>
        <FormRow>
          <TextField
            label={lang.email}
            type="email"
            data-test="email"
            onBlur={this.onBlurEmail}
            isError={this.getIsErrorForField('email')}
            value={values.email}
            onChange={this.onChangeEmail}
          />
        </FormRow>
        <FormRow>
          <TextField
            label={lang.password}
            type="password"
            data-test="password"
            onBlur={this.onBlurPassword}
            isError={this.getIsErrorForField('password')}
            value={values.password}
            onChange={this.onChangePassword}
          />
        </FormRow>
        <FormRow>
          <Button
            type="SECONDARY_DARK"
            data-test="login"
            onClick={this.onGoLogin}
          >
            {lang.login}
          </Button>
          <Button
            data-test="register"
            onClick={this.onRegister}
            isDisabled={hasAnyErrors}
          >
            {lang.register}
          </Button>
        </FormRow>
      </div>
    );
  }

  renderSuccessPage() {
    return <SuccessPage header={lang.thankYou} body={lang.registerSuccess} />;
  }

  render() {
    const registerStatus = this.props.registerStatus;

    return (
      <div>
        {registerStatus === statuses.SUCCESS
          ? this.renderSuccessPage()
          : this.renderForm()}
      </div>
    );
  }
}

export default Register;

import ValidatorHOC from '../../utils/ValidatorHOC';
import * as validations from '../../utils/validations';

export default ValidatorHOC({
  name: {
    isValid: validations.alwaysValid,
    isRequired: true,
  },
  email: {
    isValid: validations.email,
    isRequired: false,
  },
  phone: {
    isValid: validations.phone,
    isRequired: false,
  },
  body: {
    isValid: validations.alwaysValid,
    isRequired: true,
  },
  share: {
    isValid: validations.alwaysValid,
    isRequired: true,
  },
  campus: {
    isValid: validations.alwaysValid,
    isRequired: false,
  },
  notifyMe: {
    isValid: validations.alwaysValid,
    isRequired: false,
  },
});

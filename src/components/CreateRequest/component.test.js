import React from 'react';
import { shallow } from 'enzyme';

import CreateRequest from './component';

describe('<CreateRequest />', () => {
  test('it renders', () => {
    const prayerRequest = shallow(<CreateRequest />);
    expect(prayerRequest.exists()).toBe(true);
  });
});

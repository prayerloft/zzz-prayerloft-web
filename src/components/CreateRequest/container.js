import { connect } from 'react-redux';
import makeCreateStructuredSelector from '../../utils/makeCreateStructuredSelector';
import * as requestsSagas from '../../sagas/requests';
import * as entitiesSelectors from '../../selectors/entities';
import * as keys from '../../constants/keys';

const mapActions = {
  onSubmit: requestsSagas.createPrayerRequest,
};

const makeMapState = makeCreateStructuredSelector({
  status: entitiesSelectors.makeStatusForKeySelector(keys.CREATE_REQUEST),
});

export default connect(makeMapState, mapActions);

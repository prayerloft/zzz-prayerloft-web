import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import TextField from '../../elements/TextField';
import Select from '../../elements/Select';
import Button from '../../elements/Button';
import Checkbox from '../../elements/Checkbox';
import FormRow from '../../elements/FormRow';
import Header from '../../elements/Header';
import CampusSelect from '../CampusSelect';
import ValidatorField from '../../utils/ValidatorField';

import { noop } from '../../utils/defaults';
import lang from '../../lang';

import * as requestsConstants from '../../constants/requests';

const Name = ValidatorField('name')(TextField);
const Email = ValidatorField('email')(TextField);
const Phone = ValidatorField('phone')(TextField);
const Body = ValidatorField('body')(TextField);
const Share = ValidatorField('share')(Select);
const Campus = ValidatorField('campus')(CampusSelect);
const NotifyMe = ValidatorField('notifyMe', false)(Checkbox);

const shareConstants = [
  requestsConstants.SHARE_PUBLIC,
  requestsConstants.SHARE_ANONYMOUS,
  requestsConstants.SHARE_PRIVATE,
];

const shareOptions = [
  { label: 'Choose...', value: '' },
  ...shareConstants.map(constant => ({
    label: lang[constant],
    value: constant,
  })),
];

export class CreateRequest extends PureComponent {
  static propTypes = {
    onSubmit: PropTypes.func,
    values: PropTypes.object,
    status: PropTypes.string,
    hasErrors: PropTypes.bool,
    setAllFieldsDirty: PropTypes.func,
  };

  static defaultProps = {
    onSubmit: noop,
    values: {},
    status: '',
    hasErrors: false,
    setAllFieldsDirty: noop,
  };

  onSubmit = () => {
    const {
      name,
      email,
      body,
      share,
      campus,
      phone,
      notifyMe,
    } = this.props.values;
    this.props.setAllFieldsDirty();

    if (!this.props.hasErrors) {
      this.props.onSubmit({
        name,
        email,
        body,
        share,
        campus,
        phone,
        notifyMe,
      });
    }
  };

  render() {
    return (
      <div>
        <FormRow>
          <Header>{lang.createRequest}</Header>
        </FormRow>
        <FormRow>
          <Name label={`${lang.name} *`} data-test="name" />
        </FormRow>
        <FormRow halfColumn>
          <Email label={lang.email} type="email" data-test="email" />
        </FormRow>
        <FormRow halfColumn>
          <Phone label={lang.phone} type="phone" data-test="phone" />
        </FormRow>
        <FormRow>
          <Body label={`${lang.prayer} *`} data-test="body" />
        </FormRow>
        <FormRow halfColumn>
          <Share
            label={`${lang.share} *`}
            data-test="share"
            options={shareOptions}
          />
        </FormRow>
        <FormRow halfColumn>
          <Campus />
        </FormRow>
        <FormRow>
          <Button
            data-test="submit"
            onClick={this.onSubmit}
            isDisabled={this.props.hasErrors}
          >
            {lang.submit}
          </Button>
          <NotifyMe label="Notify me when someone prays for this." />
        </FormRow>
      </div>
    );
  }
}

export default CreateRequest;

import React from 'react';
import PropTypes from 'prop-types';

class SuccessPage extends React.PureComponent {
  static propTypes = {
    header: PropTypes.string,
    body: PropTypes.string,
  };

  static defaultProps = {
    header: '',
    body: '',
  };

  render() {
    return (
      <div>
        <h1>{this.props.header}</h1>
        <p>{this.props.body}</p>
      </div>
    );
  }
}

export default SuccessPage;

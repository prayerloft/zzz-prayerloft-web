import React from 'react';
import { shallow } from 'enzyme';

import SuccessPage from './component';

describe('<SuccessPage />', () => {
  test('it renders', () => {
    const wrapper = shallow(<SuccessPage />);
    expect(wrapper.exists()).toBe(true);
  });

  test('shows the header text', () => {
    const wrapper = shallow(<SuccessPage header="hello" />);
    expect(wrapper.text()).toContain('hello');
  });

  test('shows the body text', () => {
    const wrapper = shallow(<SuccessPage body="hello" />);
    expect(wrapper.text()).toContain('hello');
  });
});

import humps from 'humps';
import * as helpers from '../utils/apiHelpers';
import * as viewSelectors from '../selectors/view';

const API_BASE = process.env.API_BASE || '';

const processBody = data => JSON.stringify(humps.decamelizeKeys(data));

export default ({ url, ...options }, dispatch, getState) => {
  const shortNameSelector = viewSelectors.makeShortNameFromPayloadSelector();
  const shortName = shortNameSelector(getState());
  return fetch(`${API_BASE}/${shortName}${url}`, {
    ...options,
    credentials: 'include',
    headers: helpers.jsonHeaders(),
    body: options.body ? processBody(options.body) : null,
  })
    .then(response => {
      const contentType = response.headers.get('content-type');
      const isJSON = contentType && contentType.includes('application/json');

      const body = isJSON ? response.json() : response.text();

      if (response.ok) {
        return body;
      }

      return body.then(data =>
        Promise.reject({
          status: response.status,
          statusText: response.statusText,
          message: typeof data === 'string' ? data : data.message,
        }),
      );
    })
    .then(x => humps.camelizeKeys(x));
};

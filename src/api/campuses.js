export const getAllCampuses = () => ({
  url: '/campuses',
  method: 'GET',
});

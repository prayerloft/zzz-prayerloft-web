import * as orgApi from './org';

describe('#getByShortName', () => {
  test('it matches snapshot', () => {
    expect(orgApi.getByShortName('shortName')).toMatchSnapshot();
  });
});

import * as usersApi from './users';

const user = {
  name: 'user name',
  email: 'user email',
  password: 'user password',
};

describe('#register', () => {
  test('it matches the snapshot', () => {
    expect(usersApi.register('shortName', user)).toMatchSnapshot();
  });
});

describe('#login', () => {
  test('it matches the snapshot', () => {
    expect(usersApi.login('shortName', user)).toMatchSnapshot();
  });
});

describe('#logout', () => {
  test('it matches the snapshot', () => {
    expect(usersApi.logout('shortName')).toMatchSnapshot();
  });
});

describe('#getSession', () => {
  test('it matches the snapshot', () => {
    expect(usersApi.getSession('shortName')).toMatchSnapshot();
  });
});

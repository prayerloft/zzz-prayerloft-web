import * as prayersApi from './prayers';

describe('#prayForRequest', () => {
  test('it matches the snapshot', () => {
    expect(prayersApi.prayForRequest('shortName', 'anid')).toMatchSnapshot();
  });
});

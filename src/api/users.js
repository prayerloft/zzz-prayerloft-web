export const register = user => ({
  url: '/register',
  method: 'POST',
  body: {
    name: user.name,
    email: user.email,
    password: user.password,
  },
});

export const login = user => ({
  url: '/login',
  method: 'POST',
  body: {
    email: user.email,
    password: user.password,
  },
});

export const logout = () => ({
  url: '/login',
  method: 'DELETE',
});

export const getSession = () => ({
  url: '/login',
  method: 'GET',
});

export const getUser = id => ({
  url: `/users/${id}`,
  method: 'GET',
});

export const updateUser = (id, data) => ({
  url: `/users/${id}`,
  method: 'PUT',
  body: data,
});

export const getAllUsers = () => ({
  url: '/users',
  method: 'GET',
});

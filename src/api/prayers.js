export const prayForRequest = id => ({
  url: `/iprayed/${id}`,
  method: 'POST',
});

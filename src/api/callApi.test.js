import callApi from './callApi';
import { withViewShortName } from '../test/mockState';

const headers = {
  get: () => ['application/json'],
};

const mockResponse = {
  headers,
  json: () => Promise.resolve({ first_name: 'joe' }),
  ok: true,
};

const failResponse = {
  headers,
  ok: false,
  status: 400,
  statusText: 'Bad Request',
  json: () => Promise.resolve({ message: 'bad' }),
};

describe('#callApi', () => {
  test('should call fetch with url', async () => {
    global.fetch = jest.fn();
    global.fetch.mockReturnValueOnce(Promise.resolve(mockResponse));
    const request = { url: '/a url' };
    await callApi(request, null, withViewShortName);
    expect(global.fetch.mock.calls[0][0]).toBe('/shortName/a url');
  });

  test('should json encode body', async () => {
    global.fetch = jest.fn();
    global.fetch.mockReturnValueOnce(Promise.resolve(mockResponse));
    const request = { url: 'a url', body: { prop: true } };
    await callApi(request, null, withViewShortName);
    expect(global.fetch.mock.calls[0][1]).toHaveProperty(
      'body',
      '{"prop":true}',
    );
  });

  test('should change snake case to camel case', async () => {
    global.fetch = jest.fn();
    global.fetch.mockReturnValueOnce(Promise.resolve(mockResponse));
    const request = { url: '/a url' };
    const response = await callApi(request, null, withViewShortName);
    expect(response).toHaveProperty('firstName', 'joe');
  });

  test('should give status info on error', done => {
    global.fetch = jest.fn();
    global.fetch.mockReturnValueOnce(Promise.resolve(failResponse));
    const request = { url: '/a url' };
    callApi(request, null, withViewShortName).catch(response => {
      expect(response).toHaveProperty('status', 400);
      done();
    });
  });
});

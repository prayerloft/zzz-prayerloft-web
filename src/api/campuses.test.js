import * as campusesApi from './campuses';

describe('#getAllCampuses', () => {
  test('it matches snapshot', () => {
    expect(campusesApi.getAllCampuses('shortName')).toMatchSnapshot();
  });
});

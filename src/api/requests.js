export const getAllForOrg = () => ({
  url: '/prayers',
});

export const getActiveForOrg = () => ({
  url: '/prayers?status=ACTIVE',
});

export const getWaitingForOrg = () => ({
  url: '/prayers?status=WAITING',
});

export const createPrayerRequest = prayer => ({
  url: '/prayers',
  method: 'POST',
  body: {
    name: prayer.name,
    prayer: prayer.body,
    share_option: prayer.share,
    email: prayer.email,
    phone: prayer.phone,
    campus_id: prayer.campus,
    notify_me: prayer.notifyMe,
    contact_me: prayer.contactMe,
  },
});

export const acceptRequest = (request, user) => ({
  url: `/prayers/${request}/approve/${user}`,
});

export const deleteRequest = request => ({
  url: `/prayers/${request}`,
  method: 'DELETE',
});

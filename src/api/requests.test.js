import * as requestsApi from './requests';

describe('#getAllForOrg', () => {
  test('it matches the snapshot', () => {
    expect(requestsApi.getAllForOrg('shortName')).toMatchSnapshot();
  });
});

describe('#createPrayerRequest', () => {
  test('it matches the snapshot', () => {
    const prayer = {
      name: 'a name',
      email: 'a email',
      body: 'a body',
      share: 'PUBLIC',
      notifyMe: true,
      contactMe: false,
    };
    expect(
      requestsApi.createPrayerRequest('shortName', prayer),
    ).toMatchSnapshot();
  });
});

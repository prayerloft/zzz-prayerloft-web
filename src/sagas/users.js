import * as actions from '../ducks/users';
import * as usersApi from '../api/users';
import * as notificationsActions from '../ducks/notifications';

export const register = user => {
  return async (dispatch, getState, callApi) => {
    try {
      dispatch(actions.registerPending());
      const result = await callApi(usersApi.register(user));
      dispatch(actions.setCurrent(result.id));
      dispatch(actions.registerSuccess());
    } catch (err) {
      dispatch(actions.registerError(err));
    }
  };
};

export const getUser = id => {
  return async (dispatch, getState, callApi) => {
    try {
      const user = await callApi(usersApi.getUser(id));
      dispatch(actions.pushUser(user));
    } catch (err) {
      // no need for error handling
    }
  };
};

export const updateUser = (id, data) => {
  return async (dispatch, getState, callApi) => {
    try {
      dispatch(actions.pushUser(data));
      await callApi(usersApi.updateUser(id, data));
      dispatch(
        notificationsActions.pushSuccess({ text: 'User has been saved!' }),
      );
    } catch (err) {
      // no need for error handling
    }
  };
};

export const getAllUsers = () => {
  return async (dispatch, getState, callApi) => {
    try {
      const users = await callApi(usersApi.getAllUsers());
      users.forEach(user => dispatch(actions.pushUser(user)));
    } catch (err) {
      // no need for error handling
    }
  };
};

export const changeUserToRole = (id, role) => {
  return async (dispatch, getState, callApi) => {
    const isAdmin = role === 'ADMIN';
    const isModerator = role === 'MODERATOR';
    const data = { isAdmin, isModerator };

    try {
      const user = await callApi(usersApi.updateUser(id, data));
      dispatch(actions.pushUser(user));
    } catch (err) {
      // no need for error handling
    }
  };
};

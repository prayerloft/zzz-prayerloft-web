import * as sagas from './prayers';
import { withViewShortName } from '../test/mockState';

describe('#prayForRequest', () => {
  test('it should call mockApi with request', async done => {
    const mockApi = jest.fn();
    const dispatch = jest.fn();

    mockApi.mockReturnValueOnce(Promise.resolve());
    await sagas.prayForRequest('theid')(dispatch, withViewShortName, mockApi);

    expect(mockApi.mock.calls[0][0]).toHaveProperty('url', '/iprayed/theid');

    done();
  });
});

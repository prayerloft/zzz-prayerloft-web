import { fromCache, toCache } from '../utils/cache';

export default (request, callback) => async (dispatch, getState, callApi) => {
  const cached = fromCache(request);
  if (cached) {
    callback(cached);
  }
  const data = await callApi(request);
  toCache(request, data);
  callback(data);
};

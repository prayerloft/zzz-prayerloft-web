import * as sagas from './requests';
import { withViewShortName } from '../test/mockState';

describe('#fetchActiveRequests', () => {
  test('it should call the getActiveForOrg api', async () => {
    const mockApi = jest.fn();
    const dispatch = jest.fn();
    mockApi.mockReturnValueOnce(Promise.resolve([]));
    await sagas.fetchActiveRequests()(dispatch, withViewShortName, mockApi);

    expect(mockApi.mock.calls[0][0]).toHaveProperty(
      'url',
      '/prayers?status=ACTIVE',
    );
  });
});

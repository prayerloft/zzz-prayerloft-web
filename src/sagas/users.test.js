import * as sagas from './users';
import { withViewShortName } from '../test/mockState';

describe('#register', () => {
  test('it should call the register api with the shortName and user', async () => {
    const mockApi = jest.fn();
    const dispatch = jest.fn();
    mockApi.mockReturnValueOnce(Promise.resolve({}));
    await sagas.register({})(dispatch, withViewShortName, mockApi);

    expect(mockApi.mock.calls[0][0]).toHaveProperty('url', '/register');
  });

  test('it should dispatch error if api error', async () => {
    const mockApi = jest.fn();
    const dispatch = jest.fn();

    mockApi.mockReturnValueOnce(Promise.reject());
    await sagas.register({})(dispatch, withViewShortName, mockApi);

    expect(mockApi.mock.calls[0][0]).toHaveProperty('url', '/register');
    expect(dispatch.mock.calls[1][0]).toHaveProperty(
      'type',
      '@@users/REGISTER_ERROR',
    );
  });
});

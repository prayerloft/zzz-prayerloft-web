import * as actions from '../ducks/users';
import * as viewActions from '../ducks/view';
import * as usersApi from '../api/users';
import * as notificationsActions from '../ducks/notifications';
import * as locationSagas from './location';
import lang from '../lang';

export const login = data => {
  return async (dispatch, getState, callApi) => {
    try {
      dispatch(actions.loginPending());
      const session = await callApi(usersApi.login(data));
      const user = await callApi(usersApi.getUser(session.id));
      dispatch(actions.pushUser(user));
      dispatch(actions.setCurrent(user.id));
      dispatch(actions.loginSuccess());
      dispatch(locationSagas.navigate(viewActions.PRAYER_LIST));
    } catch (err) {
      const text =
        err.status === 401 ? lang.incorrectEmailOrPass : lang.unknownError;

      dispatch(actions.loginError(err));
      dispatch(notificationsActions.pushError({ text }));
    }
  };
};

export const getSession = () => {
  return async (dispatch, getState, callApi) => {
    try {
      const session = await callApi(usersApi.getSession());
      if (session.id) {
        const user = await callApi(usersApi.getUser(session.id));
        dispatch(actions.pushUser(user));
        dispatch(actions.setCurrent(user.id));
      }
    } catch (err) {
      // no need for error handling
    }
  };
};

export const logout = () => {
  return async (dispatch, getState, callApi) => {
    await callApi(usersApi.logout());
    dispatch(actions.setCurrent(''));
  };
};

import * as orgApi from '../api/org';

import orgSchema from '../schemas/org';
import * as entitySagas from './entities';
import callWithCache from './callWithCache';

export const getCurrent = () => dispatch => {
  const orgRequest = orgApi.getByShortName();
  dispatch(
    callWithCache(orgRequest, data => {
      dispatch(entitySagas.merge('/org', data, orgSchema));
    }),
  );
};

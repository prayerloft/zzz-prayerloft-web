import { normalize } from 'normalizr';
import * as entityActions from '../ducks/entities';
import * as metaActions from '../ducks/meta';

export const merge = (key, data, schema) => async dispatch => {
  const normalized = normalize(data, schema);
  dispatch(entityActions.merge(normalized.entities));
  dispatch(metaActions.setResult({ key, result: normalized.result }));
};

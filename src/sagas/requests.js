import * as usersSelectors from '../selectors/users';
import * as requestsApi from '../api/requests';
import * as notificationsActions from '../ducks/notifications';
import * as viewSagas from '../sagas/location';
import * as viewActions from '../ducks/view';
import prayerRequestSchema from '../schemas/prayerRequest';
import * as entitySagas from './entities';
import * as metaActions from '../ducks/meta';
import * as keys from '../constants/keys';

export const fetchActiveRequests = () => {
  return async (dispatch, getState, callApi) => {
    try {
      dispatch(metaActions.setPending({ key: keys.ACTIVE_REQUESTS }));
      const requests = await callApi(requestsApi.getActiveForOrg());
      dispatch(
        entitySagas.merge(keys.ACTIVE_REQUESTS, requests, [
          prayerRequestSchema,
        ]),
      );
      dispatch(metaActions.setSuccess({ key: keys.ACTIVE_REQUESTS }));
    } catch (err) {
      dispatch(metaActions.setError({ key: keys.ACTIVE_REQUESTS }));
    }
  };
};

export const fetchWaitingRequests = () => {
  return async (dispatch, getState, callApi) => {
    try {
      dispatch(metaActions.setPending({ key: keys.WAITING_REQUESTS }));
      const requests = await callApi(requestsApi.getWaitingForOrg());
      dispatch(
        entitySagas.merge(keys.WAITING_REQUESTS, requests, [
          prayerRequestSchema,
        ]),
      );
      dispatch(metaActions.setSuccess({ key: keys.WAITING_REQUESTS }));
    } catch (err) {
      dispatch(metaActions.setError({ key: keys.WAITING_REQUESTS }));
    }
  };
};

export const createPrayerRequest = prayerRequest => {
  return async (dispatch, getState, callApi) => {
    const key = keys.CREATE_REQUEST;
    try {
      dispatch(metaActions.setPending({ key }));
      await callApi(requestsApi.createPrayerRequest(prayerRequest));
      dispatch(metaActions.setSuccess({ key }));
      dispatch(
        notificationsActions.pushSuccess({
          text: 'Thank you for submitting your prayer request!',
        }),
      );
      dispatch(viewSagas.navigate(viewActions.PRAYER_LIST));
    } catch (err) {
      dispatch(metaActions.setError({ key }));
      dispatch(
        notificationsActions.pushError({
          text: 'Something happened...',
        }),
      );
    }
  };
};

export const acceptRequest = id => {
  const userIdSelector = usersSelectors.makeCurrentUserIdSelector();

  return async (dispatch, getState, callApi) => {
    try {
      const userId = userIdSelector(getState());
      await callApi(requestsApi.acceptRequest(id, userId));
      dispatch(
        notificationsActions.pushSuccess({
          text: 'Prayer request approved!',
        }),
      );
      dispatch(fetchWaitingRequests());
    } catch (err) {
      dispatch(
        notificationsActions.pushError({
          text: 'Something happened...',
        }),
      );
    }
  };
};

export const deleteRequest = id => {
  return async (dispatch, getState, callApi) => {
    try {
      await callApi(requestsApi.deleteRequest(id));
      dispatch(
        notificationsActions.pushSuccess({
          text: 'Prayer request deleted!',
        }),
      );
      dispatch(fetchWaitingRequests());
    } catch (err) {
      dispatch(
        notificationsActions.pushError({
          text: 'Something happened...',
        }),
      );
    }
  };
};

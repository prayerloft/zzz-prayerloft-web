import { NOT_FOUND } from 'redux-first-router';
import * as sessionSagas from './session';
import * as campusesSagas from './campuses';
import * as orgSagas from './org';

export const startApp = () => async dispatch => {
  try {
    dispatch(sessionSagas.getSession());
    dispatch(campusesSagas.getAll());
    dispatch(orgSagas.getCurrent());
  } catch (err) {
    dispatch({ type: NOT_FOUND, payload: err });
  }
};

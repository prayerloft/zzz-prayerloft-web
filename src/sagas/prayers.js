import * as prayersActions from '../ducks/prayers';
import * as prayersApi from '../api/prayers';
import delay from '../utils/delay';
import isTest from '../utils/isTest';

const PRAY_DELAY = isTest() ? 0 : 2000;

export const prayForRequest = id => {
  return async (dispatch, getState, callApi) => {
    dispatch(prayersActions.pendingPrayed(id));
    await Promise.all([
      callApi(prayersApi.prayForRequest(id)),
      delay(PRAY_DELAY),
    ]);
    dispatch(prayersActions.completedPrayed(id));
  };
};

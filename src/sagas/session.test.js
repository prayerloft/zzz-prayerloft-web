import * as sagas from './session';
import { withViewShortName } from '../test/mockState';

describe('#login', () => {
  test('it should call the login api with the shortName and user', async () => {
    const mockApi = jest.fn();
    const dispatch = jest.fn();
    mockApi.mockReturnValueOnce(Promise.resolve({}));
    await sagas.login({})(dispatch, withViewShortName, mockApi);

    expect(mockApi.mock.calls[0][0]).toHaveProperty('url', '/login');
    expect(mockApi.mock.calls[0][0]).toHaveProperty('method', 'POST');
  });

  test('it should dispatch error if api error', async () => {
    const mockApi = jest.fn();
    const dispatch = jest.fn();

    mockApi.mockReturnValueOnce(Promise.reject({ status: 400 }));
    await sagas.login({})(dispatch, withViewShortName, mockApi);

    expect(mockApi.mock.calls[0][0]).toHaveProperty('url', '/login');
    expect(dispatch.mock.calls[1][0]).toHaveProperty(
      'type',
      '@@users/LOGIN_ERROR',
    );
  });
});

describe('#getSession', () => {
  test('it should call the getSession api with the shortName', async () => {
    const mockApi = jest.fn();
    const dispatch = jest.fn();
    mockApi.mockReturnValueOnce(Promise.resolve({}));
    await sagas.getSession()(dispatch, withViewShortName, mockApi);

    expect(mockApi.mock.calls[0][0]).toHaveProperty('url', '/login');
    expect(mockApi.mock.calls[0][0]).toHaveProperty('method', 'GET');
  });
});

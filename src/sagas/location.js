import { makeEntitiesForKeySelector } from '../selectors/entities';
import orgSchema from '../schemas/org';

export const navigate = (type, payload = {}) => {
  const getOrg = makeEntitiesForKeySelector('/org', orgSchema)();
  return async (dispatch, getState) => {
    const org = getOrg(getState());
    dispatch({
      type,
      payload: {
        ...payload,
        shortName: org.get('shortName'),
      },
    });
  };
};

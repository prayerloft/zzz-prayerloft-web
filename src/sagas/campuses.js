import * as campusesApi from '../api/campuses';

import campusSchema from '../schemas/campus';
import * as entitySagas from './entities';
import callWithCache from './callWithCache';

export const getAll = () => dispatch => {
  const campusesRequest = campusesApi.getAllCampuses();
  dispatch(
    callWithCache(campusesRequest, data => {
      dispatch(entitySagas.merge('/campuses', data, [campusSchema]));
    }),
  );
};

import reducer from './users';
import * as actions from './users';
import * as statuses from '../constants/statuses';

describe('users reducer', () => {
  test('should send back state with other actions', () => {
    const action = { type: 'other', payload: {} };
    const state = {};
    const result = reducer(state, action);
    expect(result).toBe(state);
  });

  test('should set current', () => {
    const action = actions.setCurrent('1');
    const result = reducer(undefined, action);
    expect(result.get('current')).toBe('1');
  });

  test('should set register pending', () => {
    const action = actions.registerPending();
    const result = reducer(undefined, action);
    expect(result.get('registerStatus')).toBe(statuses.PENDING);
  });

  test('should set register success', () => {
    const action = actions.registerSuccess();
    const result = reducer(undefined, action);
    expect(result.get('registerStatus')).toBe(statuses.SUCCESS);
  });

  test('should set register error', () => {
    const action = actions.registerError();
    const result = reducer(undefined, action);
    expect(result.get('registerStatus')).toBe(statuses.ERROR);
  });

  test('should set login pending', () => {
    const action = actions.loginPending();
    const result = reducer(undefined, action);
    expect(result.get('loginStatus')).toBe(statuses.PENDING);
  });

  test('should set login success', () => {
    const action = actions.loginSuccess();
    const result = reducer(undefined, action);
    expect(result.get('loginStatus')).toBe(statuses.SUCCESS);
  });

  test('should set login error', () => {
    const action = actions.loginError();
    const result = reducer(undefined, action);
    expect(result.get('loginStatus')).toBe(statuses.ERROR);
  });

  test('should add user to list', () => {
    const action = actions.pushUser({ id: '1234' });
    const result = reducer(undefined, action);
    expect(result.get('ids').count()).toBe(1);
  });
});

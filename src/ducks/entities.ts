import { fromJS, Map } from 'immutable';
import { createAction, Action } from 'redux-actions';

const MERGE = '@@entities/MERGE';

export const merge = createAction(MERGE);

const initialState = Map();

export default (state = initialState, action:Action<{}>) => {
  switch (action.type) {
    case MERGE:
      return state.mergeDeep(fromJS(action.payload));
    default:
      return state;
  }
};

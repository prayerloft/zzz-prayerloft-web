import { fromJS } from 'immutable';
import reducer from './prayers';
import * as actions from './prayers';

const initialState = fromJS({});

describe('prayers reducer', () => {
  test('should send back state with other actions', () => {
    const action = { type: 'other', payload: {} };
    const state = fromJS({});
    const result = reducer(state, action);
    expect(result).toBe(state);
  });
});

import { fromJS } from 'immutable';
import { createAction } from 'redux-actions';

const PUSH = '@@notifications/PUSH';

export const push = props =>
  createAction(PUSH)({
    createdAt: Date.now(),
    duration: 5000,
    ...props,
  });

export const pushSuccess = props =>
  push({
    type: 'success',
    ...props,
  });

export const pushError = props =>
  push({
    type: 'error',
    ...props,
  });

const initialState = fromJS({
  all: [],
});

export default (state = initialState, action) => {
  switch (action.type) {
    case PUSH: {
      const all = state.get('all').push(fromJS(action.payload));
      return state.set('all', all);
    }
    default:
      return state;
  }
};

import { Map, fromJS } from 'immutable';
import { createAction, Action } from 'redux-actions';
import * as R from 'ramda';

import * as statuses from '../constants/statuses';

const SET_RESULT = '@@meta/SET_RESULT';
const SET_PENDING = '@@meta/SET_PENDING';
const SET_SUCCESS = '@@meta/SET_SUCCESS';
const SET_ERROR = '@@meta/SET_ERROR';

const passThroughPayload = (x:{}) => x;

export const setResult = createAction(SET_RESULT, passThroughPayload);
export const setPending = createAction(SET_PENDING, passThroughPayload);
export const setSuccess = createAction(SET_SUCCESS, passThroughPayload);
export const setError = createAction(SET_ERROR, passThroughPayload);

export const initialState = Map({});

const initialMeta = Map({
  status: statuses.NOT_STARTED,
  result: null,
});

const getKey = R.pathOr('', ['payload', 'key']);
const getResult = R.pathOr(null, ['payload', 'result']);

export default (state = initialState, action:Action<{}>) => {
  switch (action.type) {
    case SET_RESULT: {
      const key = getKey(action);
      const result = getResult(action);
      const currentMeta = Map(state.get(key, initialMeta));
      const newMeta = currentMeta.set('result', fromJS(result));
      return state.set(key, newMeta);
    }
    case SET_PENDING: {
      const key = getKey(action);
      const currentMeta = Map(state.get(key, initialMeta));
      const newMeta = currentMeta.set('status', statuses.PENDING);
      return state.set(key, newMeta);
    }
    case SET_SUCCESS: {
      const key = getKey(action);
      const currentMeta = Map(state.get(key, initialMeta));
      const newMeta = currentMeta.set('status', statuses.SUCCESS);
      return state.set(key, newMeta);
    }
    case SET_ERROR: {
      const key = getKey(action);
      const currentMeta = Map(state.get(key, initialMeta));
      const newMeta = currentMeta.set('status', statuses.ERROR);
      return state.set(key, newMeta);
    }
    default:
      return state;
  }
};

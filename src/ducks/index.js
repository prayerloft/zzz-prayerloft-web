import { combineReducers } from 'redux-immutable';
import prayers from './prayers';
import users from './users';
import notifications from './notifications';
import entities from './entities';
import meta from './meta';

export default combineReducers({
  prayers,
  users,
  notifications,
  entities,
  meta,
});

import { fromJS } from 'immutable';
import { createAction } from 'redux-actions';

const PENDING_PRAYED = '@@prayers/PENDING_PRAYED';
const COMPLETED_PRAYED = '@@prayers/COMPLETED_PRAYED';

export const pendingPrayed = createAction(PENDING_PRAYED);
export const completedPrayed = createAction(COMPLETED_PRAYED);

const initialState = fromJS({
  pendingPrayed: [],
  completedPrayed: [],
});

export default (state = initialState, action) => {
  switch (action.type) {
    case PENDING_PRAYED: {
      const pendingPrayed = state.get('pendingPrayed').push(action.payload);
      return state.set('pendingPrayed', pendingPrayed);
    }
    case COMPLETED_PRAYED: {
      const pendingPrayed = state
        .get('pendingPrayed')
        .filter(x => x !== action.payload);
      const completedPrayed = state.get('completedPrayed').push(action.payload);
      return state
        .set('pendingPrayed', pendingPrayed)
        .set('completedPrayed', completedPrayed);
    }
    default:
      return state;
  }
};

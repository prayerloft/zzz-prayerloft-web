import reducer from './meta';
import * as actions from './meta';
import * as statuses from '../constants/statuses';

describe('meta reducer', () => {
  test('should send back state with other actions', () => {
    const action = { type: 'other', payload: {} };
    const state = actions.initialState;
    const result = reducer(state, action);
    expect(result).toBe(state);
  });

  test('should set the result in state', () => {
    const action = actions.setResult({ key: 'myKey', result: 1 });
    const state = reducer(undefined, action);

    expect(state.getIn(['myKey', 'result'])).toBe(1);
  });

  test('should set pending', () => {
    const action = actions.setPending({ key: 'myKey' });
    const state = reducer(undefined, action);

    expect(state.getIn(['myKey', 'status'])).toBe(statuses.PENDING);
  });

  test('should set pending', () => {
    const action = actions.setSuccess({ key: 'myKey' });
    const state = reducer(undefined, action);

    expect(state.getIn(['myKey', 'status'])).toBe(statuses.SUCCESS);
  });

  test('should set pending', () => {
    const action = actions.setError({ key: 'myKey' });
    const state = reducer(undefined, action);

    expect(state.getIn(['myKey', 'status'])).toBe(statuses.ERROR);
  });
});

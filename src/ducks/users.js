import R from 'ramda';
import { fromJS } from 'immutable';
import { createAction } from 'redux-actions';
import * as statuses from '../constants/statuses';

const SET_CURRENT = '@@users/SET_CURRENT';
const PUSH_USER = '@@users/PUSH';

const REGISTER_PENDING = '@@users/REGISTER_PENDING';
const REGISTER_SUCCESS = '@@users/REGISTER_SUCCESS';
const REGISTER_ERROR = '@@users/REGISTER_ERROR';

const LOGIN_PENDING = '@@users/LOGIN_PENDING';
const LOGIN_SUCCESS = '@@users/LOGIN_SUCCESS';
const LOGIN_ERROR = '@@users/LOGIN_ERROR';

export const setCurrent = createAction(SET_CURRENT);
export const pushUser = createAction(PUSH_USER);

export const registerPending = createAction(REGISTER_PENDING);
export const registerSuccess = createAction(REGISTER_SUCCESS);
export const registerError = createAction(REGISTER_ERROR);

export const loginPending = createAction(LOGIN_PENDING);
export const loginSuccess = createAction(LOGIN_SUCCESS);
export const loginError = createAction(LOGIN_ERROR);

const initialState = fromJS({
  current: '',
  registerStatus: statuses.NOT_STARTED,
  loginStatus: statuses.NOT_STARTED,
  loginErrorCode: 0,
  all: {},
  ids: [],
});

const getPayloadStatus = R.pathOr(0, ['payload', 'status']);

export default (state = initialState, action) => {
  switch (action.type) {
    case SET_CURRENT:
      return state.set('current', action.payload);
    case PUSH_USER: {
      const user = fromJS(action.payload);
      const existing = state.get('all').get(user.get('id'));
      const allUsers = state
        .get('all')
        .set(user.get('id'), existing ? existing.merge(user) : user);
      const ids = allUsers.map(x => x.get('id'));

      return state.set('all', allUsers).set('ids', ids);
    }
    case REGISTER_PENDING:
      return state.set('registerStatus', statuses.PENDING);
    case REGISTER_ERROR:
      return state.set('registerStatus', statuses.ERROR);
    case REGISTER_SUCCESS:
      return state.set('registerStatus', statuses.SUCCESS);
    case LOGIN_PENDING:
      return state.set('loginStatus', statuses.PENDING);
    case LOGIN_ERROR:
      return state
        .set('loginStatus', statuses.ERROR)
        .set('loginErrorCode', getPayloadStatus(action));
    case LOGIN_SUCCESS:
      return state.set('loginStatus', statuses.SUCCESS);
    default:
      return state;
  }
};

import { NOT_FOUND } from 'redux-first-router';
import { createAction } from 'redux-actions';

export const PRAYER_LIST = '@@location/PRAYER_LIST';
export const PRAYER_REQUEST = '@@location/PRAYER_REQUEST';
export const LOGIN = '@@location/LOGIN';
export const LOGOUT = '@@location/LOGOUT';
export const REGISTER = '@@location/REGISTER';
export const MODERATE = '@@location/MODERATE';
export const USER = '@@location/USER';
export const ORG_SETTINGS = '@@location/ORG_SETTINGS';

export const login = createAction(LOGIN);
export const logout = createAction(LOGOUT);
export const register = createAction(REGISTER);
export const prayerList = createAction(PRAYER_LIST);
export const prayerRequest = createAction(PRAYER_REQUEST);
export const notFound = createAction(NOT_FOUND);
export const moderate = createAction(MODERATE);
export const orgSettings = createAction(ORG_SETTINGS);

export default {
  [PRAYER_LIST]: '/:shortName/',
  [PRAYER_REQUEST]: '/:shortName/request',
  [LOGIN]: '/:shortName/login',
  [LOGOUT]: '/:shortName/logout',
  [REGISTER]: '/:shortName/register',
  [MODERATE]: '/:shortName/moderate',
  [USER]: '/:shortName/users/:userId',
  [ORG_SETTINGS]: '/:shortName/org_settings',
};

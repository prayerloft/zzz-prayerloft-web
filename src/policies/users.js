export const isModerator = user => user.get('isModerator');

export const isAdmin = user => user.get('isAdmin');

export const isBasicUser = user => !isAdmin(user) && !isModerator(user);

export const isOnlyModerator = user => !isAdmin(user) && isModerator(user);

export const atLeastModerator = user => isAdmin(user) || isModerator(user);

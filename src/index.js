import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { create } from 'jss';
import preset from 'jss-preset-default';
import { JssProvider } from 'react-jss';

const jss = create(preset());

import App from './components/App';
import createStore from './createStore';
import GlobalStyles from './elements/GlobalStyles';

render(
  <Provider store={createStore(process.env.NODE_ENV)}>
    <JssProvider jss={jss}>
      <GlobalStyles>
        <App />
      </GlobalStyles>
    </JssProvider>
  </Provider>,
  document.getElementById('root'),
);

if ('serviceWorker' in navigator) {
  navigator.serviceWorker.register('/service-worker.js');
}

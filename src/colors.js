export default {
  primary: '#fafafa',
  primaryLight: '#ffffff',
  primaryDark: '#c7c7c7',

  secondary: '#2195f2',
  secondaryLight: '#6ec5ff',
  secondaryDark: '#0068bf',

  black: '#212121',
  white: '#fafafa',
  error: '#f44336',
  success: '#4CAF50',
  transparent: 'transparent',
};

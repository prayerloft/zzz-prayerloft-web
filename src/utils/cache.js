import isTest from './isTest';

const mock = {
  getItem: () => null,
  setItem: () => null,
};

const store = isTest() ? mock : global.localStorage;

export const fromCache = ref => {
  const key = JSON.stringify(ref);
  const value = store.getItem(key);
  return value ? JSON.parse(value) : null;
};

export const toCache = (ref, data) => {
  const key = JSON.stringify(ref);
  const value = JSON.stringify(data);
  store.setItem(key, value);
};

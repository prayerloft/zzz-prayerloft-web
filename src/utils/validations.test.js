import * as validations from './validations';

describe('#validations#password', () => {
  test('should accept a valid password', () => {
    expect(validations.password('correct horse battery staple')).toBe(true);
  });
});

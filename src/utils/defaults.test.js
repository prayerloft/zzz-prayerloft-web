import * as defaults from './defaults';

describe('#noop', () => {
  test('does nothing', () => {
    expect(defaults.noop).toBeInstanceOf(Function);
    expect(defaults.noop()).toBe(null);
  });
});

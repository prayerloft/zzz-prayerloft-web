export default delay =>
  new Promise(resolve => window.setTimeout(resolve, delay));

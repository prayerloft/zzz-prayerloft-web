import React from 'react';
import PropTypes from 'prop-types';

export default (fieldName, defaultValue = '') => Field =>
  class extends React.PureComponent {
    static displayName = `ValidatorField(${Field.name})`;

    static contextTypes = {
      Validator: PropTypes.shape({
        onChange: PropTypes.func,
        onFieldDirty: PropTypes.func,
        getValues: PropTypes.func,
        getValidation: PropTypes.func,
        subscribe: PropTypes.func,
        unsubscribe: PropTypes.func,
      }),
    };

    constructor(props) {
      super(props);

      this.state = {
        value: defaultValue,
        validation: {},
      };
    }

    componentWillMount() {
      const Validator = this.context.Validator;
      if (Validator) {
        Validator.subscribe(this.update);
      }
    }

    componentWillUnmount() {
      const Validator = this.context.Validator;
      if (Validator) {
        Validator.unsubscribe(this.update);
      }
    }

    update = () => {
      const Validator = this.context.Validator;
      if (Validator) {
        const values = Validator.getValues();
        const validation = Validator.getValidation();
        this.setState({
          value: values[fieldName],
          validation: validation[fieldName],
        });
      }
    };

    onChange = value => this.context.Validator.onChange(fieldName, value);
    onFieldDirty = () => this.context.Validator.onFieldDirty(fieldName);

    render() {
      return (
        <Field
          onChange={this.onChange}
          onBlur={this.onFieldDirty}
          value={this.state.value}
          isError={this.state.validation.showError}
          {...this.props}
        />
      );
    }
  };

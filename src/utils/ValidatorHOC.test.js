import React from 'react';
import { shallow } from 'enzyme';
import ValidatorHOC from './ValidatorHOC';

const Child = () => <div />;

const makeValidator = fields => {
  const El = ValidatorHOC(fields)(Child);
  return shallow(<El />);
};

describe('#ValidatorHOC', () => {
  test('it renders its child', () => {
    const wrapper = makeValidator();

    expect(wrapper.find('Child').exists()).toBe(true);
  });

  test('it renders with various field types', () => {
    const fields = {
      aField: {
        isValid: () => true,
        hasValue: () => true,
        isRequired: true,
      },
      bField: () => true,
      cField: {
        isValid: () => true,
        hasValue: () => true,
      },
      dField: {},
    };
    const wrapper = makeValidator(fields);

    expect(wrapper.find('Child').exists()).toBe(true);
  });

  test('it can set values', () => {
    const fields = {
      aField: () => true,
    };
    const wrapper = makeValidator(fields);

    wrapper.find('Child').simulate('change', 'aField', 'aValue');

    expect(wrapper.find('Child').props().values).toHaveProperty(
      'aField',
      'aValue',
    );
  });

  test('it can set field dirty', () => {
    const fields = {
      aField: () => true,
    };
    const wrapper = makeValidator(fields);

    wrapper.find('Child').simulate('fieldDirty', 'aField');

    expect(wrapper.find('Child').props().validation.aField).toHaveProperty(
      'isDirty',
      true,
    );
  });

  test('it can set field dirty twice', () => {
    const fields = {
      aField: () => true,
    };
    const wrapper = makeValidator(fields);

    wrapper.find('Child').simulate('fieldDirty', 'aField');
    wrapper.find('Child').simulate('fieldDirty', 'aField');

    expect(wrapper.find('Child').props().validation.aField).toHaveProperty(
      'isDirty',
      true,
    );
  });

  test('it can set multiple fields dirty', () => {
    const fields = {
      aField: () => true,
      bField: () => true,
    };
    const wrapper = makeValidator(fields);

    wrapper.find('Child').simulate('fieldDirty', 'aField');
    wrapper.find('Child').simulate('fieldDirty', 'bField');

    expect(wrapper.find('Child').props().validation.aField).toHaveProperty(
      'isDirty',
      true,
    );

    expect(wrapper.find('Child').props().validation.bField).toHaveProperty(
      'isDirty',
      true,
    );
  });

  test('it can take error messages', () => {
    const fields = {
      aField: () => 'an error',
    };
    const wrapper = makeValidator(fields);

    expect(wrapper.find('Child').props().validation.aField).toHaveProperty(
      'errorMessage',
      'an error',
    );
  });
});

import { createStructuredSelector } from 'reselect';

export default (selectorMap = {}) => () =>
  createStructuredSelector(
    Object.keys(selectorMap).reduce(
      (map, key) => ({
        ...map,
        [key]: selectorMap[key](),
      }),
      {},
    ),
  );

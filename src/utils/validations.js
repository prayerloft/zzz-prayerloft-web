import isEmail from 'isemail';
import libPhoneNumber from 'google-libphonenumber';

const locale = window.navigator.language.split('-')[1];
const phoneUtil = libPhoneNumber.PhoneNumberUtil.getInstance();

export const password = (value = '') => value != null && value.length > 5;

export const alwaysValid = () => true;

export const email = (value = '') => isEmail.validate(value);

export const phone = (value = '') => {
  try {
    const number = phoneUtil.parse(value, locale);
    return phoneUtil.isValidNumber(number);
  } catch (e) {
    return false;
  }
};

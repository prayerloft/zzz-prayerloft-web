import React from 'react';
import PropTypes from 'prop-types';
import { Map } from 'immutable';

const getValidatorFn = field => {
  if (typeof field === 'function') {
    return field;
  }
  if (typeof field.isValid === 'function') {
    return field.isValid;
  }
  return () => true;
};

const getHasValueFn = field => {
  if (typeof field === 'object' && typeof field.hasValue === 'function') {
    return field.hasValue;
  }

  return value => !!value;
};

const getIsRequired = field => {
  if (typeof field === 'object') {
    return !!field.isRequired;
  }

  return false;
};

const buildFields = fields =>
  Object.keys(fields).reduce((acc, fieldName) => {
    const field = fields[fieldName];

    return {
      ...acc,
      [fieldName]: {
        isValid: getValidatorFn(field),
        hasValue: getHasValueFn(field),
        isRequired: getIsRequired(field),
      },
    };
  }, {});

export default (initialFields = {}) => WrappedComponent =>
  class extends React.PureComponent {
    static displayName = `Validator(${WrappedComponent.name})`;

    static childContextTypes = {
      Validator: PropTypes.shape({
        onChange: PropTypes.func,
        onFieldDirty: PropTypes.func,
        getValues: PropTypes.func,
        getValidation: PropTypes.func,
        subscribe: PropTypes.func,
        unsubscribe: PropTypes.func,
      }),
    };

    static propTypes = {
      initialData: PropTypes.object,
    };

    static defaultProps = {
      initialData: {},
    };

    constructor(props) {
      super(props);

      const initialData =
        props.initialData instanceof Map
          ? props.initialData.toJS()
          : props.initialData;

      this.state = {
        values: initialData,
        validation: {},
        fieldNames: Object.keys(initialFields),
        dirtyFields: [],
        fields: buildFields(initialFields),
      };

      this.listeners = [];
    }

    componentWillMount() {
      this.runValidation();
    }

    componentWillReceiveProps(next) {
      if (next.initialData !== this.props.initialData) {
        const initialData =
          next.initialData instanceof Map
            ? next.initialData.toJS()
            : next.initialData;

        this.setState({ values: initialData }, this.runValidation);
      }
    }

    getChildContext() {
      return {
        Validator: {
          onChange: this.onChange,
          onFieldDirty: this.onFieldDirty,
          getValues: this.getValues,
          getValidation: this.getValidation,
          subscribe: this.subscribe,
          unsubscribe: this.unsubscribe,
        },
      };
    }

    subscribe = cb => this.listeners.push(cb);
    unsubscribe = cb => this.listeners === this.listeners.filter(x => x === cb);
    notifySubscribers = () => this.listeners.forEach(cb => cb());

    getValues = () => this.state.values;
    getValidation = () => this.state.validation;

    onChange = (fieldName, value) => {
      this.setState(
        state => ({
          values: {
            ...state.values,
            [fieldName]: value,
          },
        }),
        this.runValidation,
      );
    };

    onFieldDirty = fieldName => {
      if (!this.state.dirtyFields.includes(fieldName)) {
        this.setState(
          state => ({
            dirtyFields: [...state.dirtyFields, fieldName],
          }),
          this.runValidation,
        );
      }
    };

    onSubmit = e => {
      e.preventDefault();
    };

    setAllFieldsDirty = () => {
      Object.keys(this.state.fields).forEach(name => {
        this.onFieldDirty(name);
      });
    };

    getFieldIsDirty = fieldName => this.state.dirtyFields.includes(fieldName);

    getHasErrors() {
      const validation = this.state.validation;
      const fieldNames = Object.keys(validation);
      return fieldNames.reduce(
        (hasError, fieldName) => hasError || validation[fieldName].isError,
        false,
      );
    }

    validateField = fieldName => {
      const value = this.state.values[fieldName];
      const field = this.state.fields[fieldName];

      const result = field.isValid(value, this.state.values);
      const hasValue = !!field.hasValue(value, this.state.values);
      const isRequired = field.isRequired;
      const isDirty = this.getFieldIsDirty(fieldName);

      const isValid = result === true;
      const isError =
        (isRequired && (!isValid || !hasValue)) ||
        (!isRequired && !isValid && hasValue);

      return {
        isError,
        isValid,
        isInvalid: !isValid,
        hasValue,
        hasNoValue: !hasValue,
        isDirty,
        isClean: !isDirty,
        showError: isDirty && isError,
        errorMessage: typeof result === 'string' ? result : false,
      };
    };

    runValidation = () => {
      const validation = this.state.fieldNames.reduce(
        (acc, key) => ({
          ...acc,
          [key]: this.validateField(key),
        }),
        {},
      );

      this.setState(
        {
          validation,
        },
        this.notifySubscribers,
      );
    };

    render() {
      return (
        <form onSubmit={this.onSubmit}>
          <WrappedComponent
            onChange={this.onChange}
            onFieldDirty={this.onFieldDirty}
            values={this.state.values}
            validation={this.state.validation}
            setAllFieldsDirty={this.setAllFieldsDirty}
            hasErrors={this.getHasErrors()}
            {...this.props}
          />
        </form>
      );
    }
  };

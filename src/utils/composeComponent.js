import * as R from 'ramda';

export default imports => {
  const component = R.last(imports);
  const containers = R.map(R.prop('default'), R.init(imports));
  return containers.reduce((comp, cont) => cont(comp), component.default);
};

import makeCreateStructuredSelector from './makeCreateStructuredSelector';

describe('#makeCreateStructuredSelector', () => {
  test('it should return a function', () => {
    expect(makeCreateStructuredSelector()).toBeInstanceOf(Function);
  });

  test('its function should return a function', () => {
    expect(makeCreateStructuredSelector({})()).toBeInstanceOf(Function);
  });

  test('it should work with keys', () => {
    const fakeMakeSelector = () => () => null;

    expect(
      makeCreateStructuredSelector({
        aKey: fakeMakeSelector,
      })(),
    ).toBeInstanceOf(Function);
  });
});

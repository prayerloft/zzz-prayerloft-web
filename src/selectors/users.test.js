import * as selectors from './users';
import { fromJS, Map } from 'immutable';

const appState = fromJS({
  users: {
    all: {
      '1234': {
        id: '1234',
        name: 'bob',
      },
    },
    ids: ['1234'],
    registerStatus: 'SUCCESS',
    loginStatus: 'SUCCESS',
    loginErrorCode: 500,
    current: '1234',
  },
});

const state = { app: appState };

describe('#makeUsersDomainSelector', () => {
  test('returns users domain from state', () => {
    const result = selectors.makeUsersDomainSelector()(state);
    expect(result.get('all')).toBeInstanceOf(Map);
    expect(result.get('registerStatus')).toBe('SUCCESS');
  });
});

describe('#makeRegisterStatusSelector', () => {
  test('returns register status', () => {
    const result = selectors.makeRegisterStatusSelector()(state);
    expect(result).toBe('SUCCESS');
  });
});

describe('#makeLoginStatusSelector', () => {
  test('returns login status', () => {
    const result = selectors.makeLoginStatusSelector()(state);
    expect(result).toBe('SUCCESS');
  });
});

describe('#makeLoginErrorCodeSelector', () => {
  test('returns login error code', () => {
    const result = selectors.makeLoginErrorCodeSelector()(state);
    expect(result).toBe(500);
  });
});

describe('#makeCurrentUserIdSelector', () => {
  test('returns logged in user id', () => {
    const result = selectors.makeCurrentUserIdSelector()(state);
    expect(result).toBe('1234');
  });
});

describe('#makeAllUsersSelector', () => {
  test('returns logged in user object', () => {
    const result = selectors.makeAllUsersSelector()(state);
    expect(result.count()).toBe(1);
  });
});

describe('#makeCurrentUserSelector', () => {
  test('returns logged in user object', () => {
    const result = selectors.makeCurrentUserSelector()(state);
    expect(result.get('name')).toBe('bob');
  });
});

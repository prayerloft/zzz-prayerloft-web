import * as selectors from './view';
import * as view from '../ducks/view';

const state = {
  location: {
    type: view.PRAYER_LIST,
    payload: {
      shortName: 'orgshort',
    },
  },
};

describe('#makeCurrentViewSelector', () => {
  test('returns current view from state', () => {
    const result = selectors.makeCurrentViewSelector()(state);
    expect(result).toBe(view.PRAYER_LIST);
  });
});

describe('#makeShortNameFromPayloadSelector', () => {
  test('returns shortName from payload', () => {
    const result = selectors.makeShortNameFromPayloadSelector()(state);
    expect(result).toBe('orgshort');
  });
});

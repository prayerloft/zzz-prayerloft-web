import { createSelector } from 'reselect';
import * as appSelectors from './app';

export const makePrayersDomainSelector = () =>
  createSelector(appSelectors.app, app => app.get('prayers'));

export const makePendingPrayedSelector = () =>
  createSelector(makePrayersDomainSelector(), prayers =>
    prayers.get('pendingPrayed'),
  );

export const makeCompletedPrayedSelector = () =>
  createSelector(makePrayersDomainSelector(), prayers =>
    prayers.get('completedPrayed'),
  );

export const makePropsIdIsPendingSelector = () =>
  createSelector(
    makePrayersDomainSelector(),
    appSelectors.makeIdFromPropsSelector(),
    (prayers, id) => prayers.get('pendingPrayed').includes(id),
  );

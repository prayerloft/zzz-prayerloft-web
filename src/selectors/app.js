export const app = state => state.app;
export const makeIdFromPropsSelector = () => (state, props) => props.id;

import { createSelector } from 'reselect';
import { Map } from 'immutable';
import * as appSelectors from './app';
import * as viewSelectors from './view';

export const makeUsersDomainSelector = () =>
  createSelector(appSelectors.app, app => app.get('users'));

export const makeRegisterStatusSelector = () =>
  createSelector(makeUsersDomainSelector(), domain =>
    domain.get('registerStatus'),
  );

export const makeLoginStatusSelector = () =>
  createSelector(makeUsersDomainSelector(), domain =>
    domain.get('loginStatus'),
  );

export const makeLoginErrorCodeSelector = () =>
  createSelector(makeUsersDomainSelector(), domain =>
    domain.get('loginErrorCode'),
  );

export const makeCurrentUserIdSelector = () =>
  createSelector(makeUsersDomainSelector(), domain => domain.get('current'));

export const makeAllUsersSelector = () =>
  createSelector(makeUsersDomainSelector(), domain =>
    domain.get('ids').map(id => domain.get('all').get(id)),
  );

export const makeCurrentUserSelector = () =>
  createSelector(
    makeAllUsersSelector(),
    makeCurrentUserIdSelector(),
    (users, userId) =>
      users.find(user => user.get('id') === userId, null, Map()),
  );

export const makeUserFromPropsId = () =>
  createSelector(
    makeAllUsersSelector(),
    viewSelectors.makeUserIdFromPayloadSelector(),
    (users, userId) =>
      users.find(user => user.get('id') === userId, null, Map()),
  );

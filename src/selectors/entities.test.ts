import * as selectors from './entities';
import { fromJS, List } from 'immutable';
import campusesSchema from '../schemas/campus';

const appState = fromJS({
  entities: {
    campuses: {
      1: {
        name: 'my campus',
      },
    },
  },
  meta: {
    '/campuses': {
      result: [1],
    },
  },
});

const state = { app: appState };

describe('#makePropsKeySelector', () => {
  test('gets the value of the prop', () => {
    const result = selectors.makePropsKeySelector('id')(state, { id: '5' });
    expect(result).toBe('5');
  });
});

describe('#makeEntitiesSelector', () => {
  test('gets the entities domain', () => {
    const result = selectors.makeEntitiesSelector()(state);
    expect(result).toBe(appState.get('entities'));
  });
});

describe('#makeMetaSelector', () => {
  test('gets the meta domain', () => {
    const result = selectors.makeMetaSelector()(state);
    expect(result).toBe(appState.get('meta'));
  });
});

describe('#makeMetaKeySelector', () => {
  test('gets the meta object associated with the key', () => {
    const result = selectors.makeMetaKeySelector('/campuses')()(state);
    expect(result).toBe(appState.getIn(['meta', '/campuses']));
  });
});

describe('#makeResultForKeySelector', () => {
  test('gets the result associated with the key', () => {
    const result = selectors.makeResultForKeySelector('/campuses')()(state);
    expect(result).toBe(appState.getIn(['meta', '/campuses', 'result']));
  });
});

describe('#makeEntitiesForKeySelector', () => {
  test('gets entities for a result key', () => {
    const result = selectors.makeEntitiesForKeySelector('/campuses', campusesSchema)()(state);
    expect(result).toBeInstanceOf(List);
    expect(result.size).toBe(1);
  });
});

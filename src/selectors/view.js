import { createSelector } from 'reselect';

export const makeLocationSelector = () => state => state.location;

export const makeCurrentViewSelector = () =>
  createSelector(makeLocationSelector(), location => location.type);

export const makeShortNameFromPayloadSelector = () =>
  createSelector(
    makeLocationSelector(),
    location => location.payload.shortName,
  );

export const makeUserIdFromPayloadSelector = () =>
  createSelector(makeLocationSelector(), location => location.payload.userId);

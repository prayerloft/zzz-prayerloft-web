import { createSelector } from 'reselect';
import { denormalize, schema } from 'normalizr';
import { Map } from 'immutable';

import * as appSelectors from './app';
import * as statuses from '../constants/statuses';

type Entity = schema.Entity;

interface Props {
  [key:string]: string;
}

type Entities = Map<string, any>;

export const makePropsKeySelector = (key:string) =>
  (state:{}, props:Props) => props[key];

export const makeEntitiesSelector = () =>
  createSelector(appSelectors.app, app => app.get('entities'));

export const makeMetaSelector = () =>
  createSelector(appSelectors.app, app => app.get('meta'));

export const makeMetaKeySelector = (key:string) => () =>
  createSelector(makeMetaSelector(), meta =>
    meta.get(key, Map()),
  );

export const makeResultForKeySelector = (key:string) => () =>
  createSelector(makeMetaSelector(), meta =>
    meta.get(key, Map()).get('result'),
  );

const denormalizeWithSchema = (schema:Entity) =>
  (entities:Entities, result:any) =>
    denormalize(result, schema, entities);

export const makeEntitiesForKeySelector = (key:string, schema:Entity) => () =>
  createSelector(
    makeEntitiesSelector(),
    makeResultForKeySelector(key)(),
    denormalizeWithSchema(schema),
  );

export const makeEntityForPropsIdSelector = (schema:Entity) => () =>
  createSelector(
    makeEntitiesSelector(),
    makePropsKeySelector('id'),
    denormalizeWithSchema(schema),
  );

export const makeStatusForKeySelector = (key:string) => () =>
  createSelector(
    makeMetaKeySelector(key)(),
    (meta) => {
      return meta.get('status', statuses.NOT_STARTED);
    }
  );

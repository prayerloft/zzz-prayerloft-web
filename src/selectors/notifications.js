import { createSelector } from 'reselect';
import * as appSelectors from './app';

export const makeNotificationsDomainSelector = () =>
  createSelector(appSelectors.app, app => app.get('notifications'));

export const makeAllNotificationsSelector = () =>
  createSelector(makeNotificationsDomainSelector(), domain =>
    domain.get('all'),
  );

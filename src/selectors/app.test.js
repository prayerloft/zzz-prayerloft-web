import * as selectors from './app';

const state = {};

describe('#makeIdFromPropsSelector', () => {
  test('returns id from props', () => {
    const result = selectors.makeIdFromPropsSelector()(state, { id: '5' });
    expect(result).toBe('5');
  });
});

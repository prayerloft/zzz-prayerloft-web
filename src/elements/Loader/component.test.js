import React from 'react';
import { shallow } from 'enzyme';

import Loader from './component';

describe('<Loader />', () => {
  test('it renders', () => {
    const wrapper = shallow(<Loader />);
    expect(wrapper.exists()).toBe(true);
  });
});

import React from 'react';
import { storiesOf } from '@storybook/react';
import globalStylesDecorator from '../../../stories/globalStylesDecorator';

import Loader from './';

storiesOf('Loader', module)
  .addDecorator(globalStylesDecorator)
  .add('basic', () => <Loader />);

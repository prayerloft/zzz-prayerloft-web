import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

export class Header extends PureComponent {
  static propTypes = {
    children: PropTypes.node,
    classes: PropTypes.object,
    size: PropTypes.oneOf(['h1', 'h2', 'h3', 'h4']),
  };

  static defaultProps = {
    children: null,
    size: 'h1',
  };

  render() {
    const classes = this.props.classes || {};
    const size = this.props.size;

    const headerClass = classnames(classes.header, classes[size]);

    return <h1 className={headerClass}>{this.props.children}</h1>;
  }
}

export default Header;

import React from 'react';
import { storiesOf } from '@storybook/react';
import globalStylesDecorator from '../../../stories/globalStylesDecorator';

import Header from './';

storiesOf('Header', module)
  .addDecorator(globalStylesDecorator)
  .add('h1', () => <Header size="h1">Hello World</Header>)
  .add('h2', () => <Header size="h2">Hello World</Header>)
  .add('h3', () => <Header size="h3">Hello World</Header>)
  .add('h4', () => <Header size="h4">Hello World</Header>);

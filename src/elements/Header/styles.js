import injectSheet from 'react-jss';

export default injectSheet({
  header: {
    display: 'inline-block',
    fontWeight: '500',
  },
  h1: {
    fontSize: '32px',
  },
  h2: {
    fontSize: '24px',
  },
  h3: {
    fontSize: '18px',
  },
  h4: {
    fontWeight: '400',
    fontSize: '16px',
    margin: '0',
  },
});

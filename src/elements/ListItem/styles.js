import injectSheet from 'react-jss';
import colors from '../../colors';

export default injectSheet({
  listItem: {
    padding: '16px 0',
    color: colors.black,
    position: 'relative',
    transform: 'translate3d(0,0,0)',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  mainContent: {},
  rightContent: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  clickable: {
    cursor: 'pointer',
  },
});

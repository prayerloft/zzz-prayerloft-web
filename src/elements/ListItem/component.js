import React from 'react';
import PropTypes from 'prop-types';
import { noop } from '../../utils/defaults';
import classnames from 'classnames';

class ListItem extends React.PureComponent {
  static propTypes = {
    children: PropTypes.node,
    rightContent: PropTypes.node,
    classes: PropTypes.object,
    onClick: PropTypes.func,
  };

  static defaultProps = {
    children: null,
    rightContent: null,
    onClick: noop,
  };

  renderRightContent() {
    const content = this.props.rightContent;
    const classes = this.props.classes || {};

    if (content) {
      return <div className={classes.rightContent}>{content}</div>;
    }
    return null;
  }

  renderMainContent() {
    const content = this.props.children;
    const classes = this.props.classes || {};

    if (content) {
      return <div className={classes.mainContent}>{content}</div>;
    }
    return null;
  }

  render() {
    const classes = this.props.classes || {};

    const listItemClasses = classnames({
      [classes.listItem]: true,
      [classes.clickable]: this.props.onClick !== noop,
    });

    return (
      <div onClick={this.props.onClick} className={listItemClasses}>
        {this.renderMainContent()}
        {this.renderRightContent()}
      </div>
    );
  }
}

export default ListItem;

import React from 'react';
import { shallow } from 'enzyme';

import ListItem from './component';

describe('<ListItem />', () => {
  test('it renders', () => {
    const wrapper = shallow(<ListItem />);
    expect(wrapper.exists()).toBe(true);
  });
});

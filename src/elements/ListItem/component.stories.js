import React from 'react';
import { storiesOf } from '@storybook/react';
import { withKnobs, text } from '@storybook/addon-knobs';
import globalStylesDecorator from '../../../stories/globalStylesDecorator';

import ListItem from './';
import Header from '../Header';
import Text from '../Text';
import Button from '../Button';

storiesOf('ListItem', module)
  .addDecorator(globalStylesDecorator)
  .addDecorator(withKnobs)
  .add('with text', () => <ListItem>{text('text', 'hello')}</ListItem>)
  .add('with header, text and button', () => (
    <ListItem rightContent={<Button>Foo Bar</Button>}>
      <Header size="h4">Header</Header>
      <Text>
        Bacon ipsum dolor amet burgdoggen beef short ribs, frankfurter pork loin
        alcatra ham hock t-bone. Beef swine jerky, pork tenderloin prosciutto
        chuck chicken bresaola spare ribs. Fatback porchetta t-bone salami swine
        beef ribs ham. Hamburger beef pastrami landjaeger alcatra, leberkas
        shank chicken short loin.
      </Text>
    </ListItem>
  ));

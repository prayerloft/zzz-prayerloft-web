import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { noop } from '../../utils/defaults';

class Button extends PureComponent {
  static propTypes = {
    children: PropTypes.node,
    onClick: PropTypes.func,
    type: PropTypes.oneOf(['PRIMARY', 'SECONDARY', 'SECONDARY_DARK']),
    fullWidth: PropTypes.bool,
    noSidePadding: PropTypes.bool,
    classes: PropTypes.object,
  };

  static defaultProps = {
    children: null,
    onClick: noop,
    type: 'PRIMARY',
    fullWidth: false,
    noSidePadding: false,
  };

  getButtonClasses() {
    if (!this.props.classes) return '';

    const classes = this.props.classes;
    const type = this.props.type;
    const fullWidth = this.props.fullWidth;
    const noSidePadding = this.props.noSidePadding;

    return classNames(classes.button, {
      [classes.primary]: type === 'PRIMARY',
      [classes.secondary]: type === 'SECONDARY',
      [classes.secondaryDark]: type === 'SECONDARY_DARK',
      [classes.fullWidth]: fullWidth,
      [classes.noSidePadding]: noSidePadding,
    });
  }

  render() {
    const { onClick, children } = this.props;

    return (
      <button onClick={onClick} className={this.getButtonClasses()}>
        {children}
      </button>
    );
  }
}

export default Button;

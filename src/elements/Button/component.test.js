import React from 'react';
import { shallow } from 'enzyme';

import Button from './component';

describe('<Button />', () => {
  test('it renders', () => {
    const wrapper = shallow(<Button />);
    expect(wrapper.exists()).toBe(true);
  });

  test('it clicks', () => {
    const onClick = jest.fn();
    const wrapper = shallow(<Button onClick={onClick} />);
    wrapper.find('button').simulate('click');
    expect(onClick).toBeCalled();
  });

  test('it shows text', () => {
    const wrapper = shallow(<Button>hello</Button>);
    expect(wrapper.text()).toBe('hello');
  });
});

import injectSheet from 'react-jss';
import colors from '../../colors';

export default injectSheet({
  button: {
    outline: 'none',
    appearance: 'none',
    backgroundColor: colors.secondary,
    border: 'none',
    color: colors.white,
    fontSize: '16px',
    fontWeight: '300',
    minWidth: '24px',
    borderRadius: '2px',
    cursor: 'pointer',
    boxShadow: `0 0 5px ${colors.primaryDark}`,
    padding: '8px 16px',
    whiteSpace: 'nowrap',
    display: 'flex',
    alignItems: 'center',
    transition: `box-shadow 200ms ease-in-out,
      background-color 200ms ease-in-out,
      color 200ms ease-in-out`,
    '&:hover': {
      boxShadow: `0 1px 5px 1px ${colors.primaryDark}`,
    },
    '&:active, &:disabled': {
      color: colors.primary,
      backgroundColor: colors.primaryDark,
    },
  },
  secondary: {
    backgroundColor: 'transparent',
    color: colors.primaryLight,
    boxShadow: 'none',
    '&:hover': {
      boxShadow: `none`,
    },
    '&:active, &:disabled': {
      color: colors.primaryDark,
      backgroundColor: colors.primary,
    },
  },
  secondaryDark: {
    backgroundColor: 'transparent',
    color: colors.black,
    boxShadow: 'none',
    '&:hover': {
      boxShadow: `none`,
    },
    '&:active, &:disabled': {
      color: colors.black,
      backgroundColor: colors.primaryDark,
    },
  },
  fullWidth: {
    width: '100%',
  },
  noSidePadding: {
    padding: '8px 0',
  },
});

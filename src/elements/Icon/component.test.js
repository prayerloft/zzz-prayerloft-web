import React from 'react';
import { shallow } from 'enzyme';

import { Icon } from './component';

describe('<Icon />', () => {
  test('it renders', () => {
    const wrapper = shallow(<Icon classes={{}} />);
    expect(wrapper.exists()).toBe(true);
  });
});

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import injectSheet from 'react-jss';
import colors from '../../colors';

const styles = {
  icon: {
    color: colors.black,
  },
  big: {
    fontSize: '32px',
  },
  selected: {
    color: colors.secondary,
  },
};

export class Icon extends PureComponent {
  static propTypes = {
    icon: PropTypes.string,
    big: PropTypes.bool,
    classes: PropTypes.object,
    selected: PropTypes.bool,
  };

  static defaultProps = {
    icon: '',
    big: false,
    selected: false,
  };

  getClasses() {
    const classes = this.props.classes || {};
    return [
      'material-icons',
      this.props.classes.icon,
      this.props.selected ? classes.selected : '',
      this.props.big ? classes.big : '',
    ].join(' ');
  }

  render() {
    return (
      <i className={this.getClasses()} aria-hidden="true">
        {this.props.icon}
      </i>
    );
  }
}

export default injectSheet(styles)(Icon);

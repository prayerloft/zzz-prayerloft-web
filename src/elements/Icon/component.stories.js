import React from 'react';
import { storiesOf } from '@storybook/react';
import globalStylesDecorator from '../../../stories/globalStylesDecorator';

import Icon from './';

storiesOf('Icon', module)
  .addDecorator(globalStylesDecorator)
  .add('list icon', () => <Icon icon="list" />)
  .add('big list icon', () => <Icon big icon="list" />);

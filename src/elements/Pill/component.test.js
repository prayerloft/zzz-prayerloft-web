import React from 'react';
import { shallow } from 'enzyme';

import { Pill } from './component';

describe('<Pill />', () => {
  test('it renders', () => {
    const wrapper = shallow(<Pill classes={{}} />);
    expect(wrapper.exists()).toBe(true);
  });
});

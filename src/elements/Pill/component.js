import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

export class Pill extends PureComponent {
  static propTypes = {
    type: PropTypes.string,
    children: PropTypes.node,
    classes: PropTypes.object,
  };

  static defaultProps = {
    type: '',
    children: null,
  };

  render() {
    const classes = this.props.classes || {};

    return <div className={classes.pill}>{this.props.children}</div>;
  }
}

export default Pill;

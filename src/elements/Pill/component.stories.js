import React from 'react';
import { storiesOf } from '@storybook/react';
import globalStylesDecorator from '../../../stories/globalStylesDecorator';

import Pill from './';

storiesOf('Pill', module)
  .addDecorator(globalStylesDecorator)
  .add('with text', () => <Pill>Hello World</Pill>);

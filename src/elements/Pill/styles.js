import colors from '../../colors';
import injectSheet from 'react-jss';

export default injectSheet({
  pill: {
    margin: '0 8px',
    fontWeight: '300',
    backgroundColor: colors.secondaryLight,
    color: colors.white,
    padding: '2px 4px',
    borderRadius: '3px',
    fontSize: '12px',
    display: 'inline-block',
  },
});

import injectSheet from 'react-jss';
import colors from '../../colors';

const styles = injectSheet({
  '@global': {
    html: {
      margin: 0,
      padding: 0,
      fontFamily: ['"Roboto"', 'sans-serif'],
      boxSizing: 'border-box',
      backgroundColor: colors.primary,
    },
    body: {
      margin: 0,
      padding: 0,
      '& #root': {
        display: 'flex',
        justifyContent: 'center',
      },
    },
    '*, *::before, *::after': {
      boxSizing: 'inherit',
    },
    'input, textarea, select, button': {
      fontFamily: ['"Roboto"', 'sans-serif'],
    },
  },
});

const GlobalStyles = ({ children }) => children;

export default styles(GlobalStyles);

import React from 'react';
import { shallow } from 'enzyme';

import { FormRow } from './component';

describe('<FormRow />', () => {
  test('it renders', () => {
    const wrapper = shallow(<FormRow />);
    expect(wrapper.exists()).toBe(true);
  });
});

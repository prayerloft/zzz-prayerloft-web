import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import injectSheet from 'react-jss';
import classnames from 'classnames';

const styles = {
  formRow: {
    display: 'flex',
    alignItems: 'center',
    '& + &': {
      marginTop: '16px',
    },
  },
  halfColumn: {
    '@media (min-width: 480px)': {
      display: 'inline-block',
      width: '50%',
    },
  },
};

export class FormRow extends PureComponent {
  static propTypes = {
    children: PropTypes.node,
    classes: PropTypes.object,
    halfColumn: PropTypes.bool,
    justify: PropTypes.string,
  };

  static defaultProps = {
    children: null,
    halfColumn: false,
    justify: '',
  };

  getClasses() {
    const classes = this.props.classes || {};
    return classnames(classes.formRow, {
      [classes.halfColumn]: this.props.halfColumn,
    });
  }

  render() {
    return (
      <div
        style={{ justifyContent: this.props.justify }}
        className={this.getClasses()}
      >
        {this.props.children}
      </div>
    );
  }
}

export default injectSheet(styles)(FormRow);

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import { noop } from '../../utils/defaults';

export class TextField extends PureComponent {
  static propTypes = {
    onBlur: PropTypes.func,
    onChange: PropTypes.func,
    value: PropTypes.string,
    type: PropTypes.string,
    label: PropTypes.string,
    isError: PropTypes.bool,
    isReadOnly: PropTypes.bool,
    classes: PropTypes.object,
    autoFocus: PropTypes.bool,
  };

  static defaultProps = {
    onBlur: noop,
    onChange: noop,
    value: '',
    type: 'text',
    label: '',
    isError: false,
    isReadOnly: false,
    autoFocus: false,
  };

  constructor(props) {
    super(props);

    this.state = {
      isFocused: false,
    };
  }

  onChange = event => {
    this.props.onChange(event.target.value);
  };

  onFocus = () => {
    this.setState({ isFocused: true });
  };

  onBlur = () => {
    this.setState({ isFocused: false });
    this.props.onBlur();
  };

  getInputId() {
    return `${this.props.label}`;
  }

  getInputClasses() {
    const { classes, isError, isReadOnly } = this.props;

    if (!classes) return '';

    return classnames(classes.textField, {
      [classes.error]: isError,
      [classes.readOnly]: isReadOnly,
    });
  }

  getLabelClasses() {
    const { classes, isError, isReadOnly } = this.props;
    const { isFocused } = this.state;

    if (!classes) return '';

    return classnames(classes.label, {
      [classes.labelError]: isError,
      [classes.labelFocus]: isFocused && !isReadOnly,
    });
  }

  render() {
    const id = this.getInputId();
    const classes = this.props.classes || {};

    return (
      <div className={classes.root}>
        <label htmlFor={id} className={this.getLabelClasses()}>
          {this.props.label}
        </label>
        <input
          id={id}
          type={this.props.type}
          className={this.getInputClasses()}
          value={this.props.value}
          onChange={this.onChange}
          onFocus={this.onFocus}
          onBlur={this.onBlur}
          readOnly={this.props.isReadOnly}
          autoFocus={this.props.autoFocus}
        />
      </div>
    );
  }
}

export default TextField;

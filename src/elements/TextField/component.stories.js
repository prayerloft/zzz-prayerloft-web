import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import globalStylesDecorator from '../../../stories/globalStylesDecorator';
import TextField from './';

storiesOf('TextField', module)
  .addDecorator(globalStylesDecorator)
  .add('empty', () => (
    <TextField label="The label" onChange={action('onChange')} />
  ))
  .add('with value', () => (
    <TextField label="The label" value="hello" onChange={action('onChange')} />
  ))
  .add('readonly', () => (
    <TextField
      isReadOnly
      label="The label"
      value="hello"
      onChange={action('onChange')}
    />
  ));

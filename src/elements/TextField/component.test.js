import React from 'react';
import sinon from 'sinon';
import { shallow } from 'enzyme';

import TextField from './component';

describe('<TextField />', () => {
  test('it renders', () => {
    const wrapper = shallow(<TextField />);
    expect(wrapper.exists()).toBe(true);
  });

  test('it blurs', () => {
    const onBlur = sinon.spy();
    const wrapper = shallow(<TextField onBlur={onBlur} />);
    wrapper.find('input').simulate('blur');
    expect(onBlur.calledOnce).toBe(true);
  });

  test('it changes', () => {
    const onChange = sinon.spy();
    const wrapper = shallow(<TextField onChange={onChange} />);
    wrapper.find('input').simulate('change', { target: { value: 'hello' } });
    expect(onChange.calledOnce).toBe(true);
    expect(onChange.calledWith('hello')).toBe(true);
  });

  test('it adds and removes focus state', () => {
    const wrapper = shallow(<TextField />);

    wrapper.find('input').simulate('focus');
    expect(wrapper.state()).toHaveProperty('isFocused', true);

    wrapper.find('input').simulate('blur');
    expect(wrapper.state()).toHaveProperty('isFocused', false);
  });

  test('it shows the value you give it', () => {
    const wrapper = shallow(<TextField value="hello" />);

    expect(wrapper.find('input').props()).toHaveProperty('value', 'hello');
  });
});

import injectSheet from 'react-jss';
import colors from '../../colors';

export default injectSheet({
  root: {
    width: '100%',
    fontWeight: '300',
  },
  textField: {
    fontWeight: '300',
    fontSize: '18px',
    width: '100%',
    border: 'none',
    backgroundColor: 'transparent',
    borderBottom: `2px solid ${colors.primaryDark}`,
    marginBottom: '1px',
    padding: '8px',
    outline: 'none',
    color: colors.black,
    borderRadius: 0,
    transition: [
      {
        property: 'color',
        duration: '100ms',
        timingFunction: 'ease-in-out',
      },
      {
        property: 'border',
        duration: '100ms',
        timingFunction: 'ease-in-out',
      },
      {
        property: 'margin',
        duration: '100ms',
        timingFunction: 'ease-in-out',
      },
    ],
    '&:focus': {
      borderBottom: `3px solid ${colors.black}`,
      marginBottom: 0,
    },
  },
  error: {
    color: colors.error,
    borderBottomColor: colors.error,
    '&:focus': {
      borderBottom: `3px solid ${colors.error}`,
      marginBottom: 0,
    },
  },
  readOnly: {
    borderBottomColor: colors.transparent,
    '&:focus': {
      borderBottom: `3px solid ${colors.transparent}`,
      marginBottom: 0,
    },
  },
  label: {
    fontWeight: '300',
    color: colors.primaryDark,
    fontSize: '16px',
    transition: [
      {
        property: 'color',
        duration: '100ms',
        timingFunction: 'ease-in-out',
      },
    ],
  },
  labelFocus: {
    color: colors.black,
  },
  labelError: {
    color: colors.error,
  },
});

import injectSheet from 'react-jss';
import colors from '../../colors';

export default injectSheet({
  menu: {
    padding: '4px 0',
    backgroundColor: colors.white,
    border: `1px solid ${colors.primaryDark}`,
    borderRadius: '2px',
  },
  item: {
    fontWeight: '300',
    padding: '8px 16px',
    minWidth: '80px',
    cursor: 'pointer',
  },
});

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import ReactModal from 'react-modal';
import Button from '../Button';
import { noop } from '../../utils/defaults';

const overlayStyle = {
  userSelect: 'none',
  backgroundColor: 'none',
};

class Menu extends PureComponent {
  static propTypes = {
    items: PropTypes.array,
    onClick: PropTypes.func,
    children: PropTypes.node,
    xOffset: PropTypes.number,
    yOffset: PropTypes.number,
    classes: PropTypes.object,
  };

  static defaultProps = {
    items: [],
    onClick: noop,
    children: null,
    xOffset: 0,
    yOffset: 0,
  };

  constructor(props) {
    super(props);
    this.state = {
      xPosition: 0,
      yPosition: 0,
      isOpen: false,
    };
  }

  root = null;

  onAfterOpen = () => {
    if (!this.root) return;
    const { xOffset, yOffset } = this.props;
    const rect = this.root.getBoundingClientRect();
    this.setState({
      xPosition: rect.left + xOffset,
      yPosition: rect.bottom + yOffset,
    });
  };

  onClick = value => () => {
    this.props.onClick(value);
  };

  setRootRef = r => {
    this.root = r;
  };

  getStyle() {
    return {
      overlay: overlayStyle,
      content: {
        position: 'absolute',
        top: this.state.yPosition,
        left: this.state.xPosition,
        right: 'auto',
        bottom: 'auto',
        border: 'none',
        background: 'transparent',
        borderRadius: 0,
        padding: 0,
      },
    };
  }

  onOpen = event => {
    event.stopPropagation();
    this.setState({ isOpen: true });
  };
  onClose = () => this.setState({ isOpen: false });

  renderItem = item => (
    <Button
      fullWidth
      type="SECONDARY_DARK"
      key={item.value}
      onClick={this.onClick(item.value)}
    >
      {item.name}
    </Button>
  );

  render() {
    const { items, children } = this.props;

    return (
      <div ref={this.setRootRef}>
        <div onClick={this.onOpen}>{children}</div>
        <ReactModal
          onAfterOpen={this.onAfterOpen}
          style={this.getStyle()}
          isOpen={this.state.isOpen}
          onRequestClose={this.onClose}
          contentLabel="menu"
        >
          <div onClick={this.onClose} className={this.props.classes.menu}>
            {items.map(this.renderItem)}
          </div>
        </ReactModal>
      </div>
    );
  }
}

export default Menu;

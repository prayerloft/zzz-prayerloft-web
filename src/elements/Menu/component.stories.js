import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import globalStylesDecorator from '../../../stories/globalStylesDecorator';

import Button from '../Button';
import Menu from './';

storiesOf('Menu', module)
  .addDecorator(globalStylesDecorator)
  .add('closed', () => (
    <Menu
      items={[{ name: 'Item  1', value: '1' }]}
      onRequestClose={action('requestClose')}
      onClick={action('onClickMenu')}
    >
      <Button onClick={action('onClickButton')}>Menu</Button>
    </Menu>
  ))
  .add('open', () => (
    <Menu
      isOpen
      items={[{ name: 'Item  1', value: '1' }]}
      onRequestClose={action('requestClose')}
      onClick={action('onClickMenu')}
    >
      <Button onClick={action('onClickButton')}>Menu</Button>
    </Menu>
  ));

import React from 'react';
import { shallow } from 'enzyme';

import Menu from './component';

const createMenu = props => shallow(<Menu classes={{}} {...props} />);

describe('<Menu />', () => {
  test('it renders', () => {
    const wrapper = createMenu();
    expect(wrapper.exists()).toBe(true);
  });

  test('it renders items', () => {
    const wrapper = createMenu({
      items: [
        { name: 'hello', value: 'hello' },
        { name: 'world', value: 'world' },
      ],
    });
    expect(wrapper.exists()).toBe(true);
  });
});

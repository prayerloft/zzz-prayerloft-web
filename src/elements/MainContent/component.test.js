import React from 'react';
import { shallow } from 'enzyme';

import { MainContent } from './component';

describe('<MainContent />', () => {
  test('it renders', () => {
    const mainContent = shallow(<MainContent />);
    expect(mainContent.exists()).toBe(true);
  });
});

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import injectSheet from 'react-jss';
import colors from '../../colors';

const styles = {
  mainContent: {
    maxWidth: '768px',
    flexBasis: '100%',
    minHeight: 'calc(100vh - 128px)',
    backgroundColor: colors.primaryLight,
    padding: '0 16px',
    marginTop: '64px',
    overflowY: 'auto',
    '@media (min-width: 768px)': {
      borderLeft: `1px solid ${colors.primaryDark}`,
      borderRight: `1px solid ${colors.primaryDark}`,
    },
    marginBottom: '64px',
    position: 'relative',
  },
};

export class MainContent extends PureComponent {
  static propTypes = {
    children: PropTypes.node,
    classes: PropTypes.object,
  };

  static defaultProps = {
    children: null,
  };

  getClasses() {
    const classes = this.props.classes;
    return classes ? classes.mainContent : '';
  }

  render() {
    return <div className={this.getClasses()}>{this.props.children}</div>;
  }
}

export default injectSheet(styles)(MainContent);

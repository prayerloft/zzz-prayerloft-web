import injectSheet from 'react-jss';
import colors from '../../colors';

export default injectSheet({
  notification: {
    fontWeight: '300',
    color: colors.white,
    height: '48px',
    fontSize: '16px',
    lineHeight: '48px',
    padding: '0 16px',
    margin: '0 -16px',
  },
  error: {
    backgroundColor: colors.error,
  },
  info: {
    backgroundColor: colors.secondary,
  },
  success: {
    backgroundColor: colors.success,
  },
});

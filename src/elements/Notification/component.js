import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

export class Notification extends PureComponent {
  static propTypes = {
    children: PropTypes.node,
    classes: PropTypes.object,
    type: PropTypes.oneOf(['error', 'info', 'success']),
  };

  static defaultProps = {
    children: null,
    type: 'info',
  };

  getClasses() {
    const classes = this.props.classes || {};
    return classnames({
      [classes.notification]: true,
      [classes[this.props.type]]: true,
    });
  }

  render() {
    return <div className={this.getClasses()}>{this.props.children}</div>;
  }
}

export default Notification;

import React from 'react';
import { shallow } from 'enzyme';

import Notification from './component.js';

describe('<Notification />', () => {
  test('it renders', () => {
    const wrapper = shallow(<Notification />);
    expect(wrapper.exists()).toBe(true);
  });

  test('it renders text', () => {
    const wrapper = shallow(<Notification>an error!</Notification>);
    expect(wrapper.text()).toBe('an error!');
  });
});

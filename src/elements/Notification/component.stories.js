import React from 'react';
import { storiesOf } from '@storybook/react';
import globalStylesDecorator from '../../../stories/globalStylesDecorator';

import Notification from './';

storiesOf('Notification', module)
  .addDecorator(globalStylesDecorator)
  .add('info / default', () => <Notification>Hello World</Notification>)
  .add('success', () => <Notification type="success">Hello World</Notification>)
  .add('error', () => <Notification type="error">Hello World</Notification>);

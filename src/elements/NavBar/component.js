import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

class NavBar extends React.PureComponent {
  static propTypes = {
    classes: PropTypes.object,
    children: PropTypes.node,
    layout: PropTypes.oneOf(['space-between', 'space-around']),
    anchor: PropTypes.oneOf(['top', 'bottom']),
  };

  getClasses() {
    const classes = this.props.classes || {};
    const { layout, anchor } = this.props;

    return classnames({
      [classes.navBar]: true,
      [classes.top]: anchor === 'top',
      [classes.bottom]: anchor === 'bottom',
      [classes.spaceBetween]: layout === 'space-between',
      [classes.spaceAround]: layout === 'space-around',
    });
  }

  render() {
    return <div className={this.getClasses()}>{this.props.children}</div>;
  }
}

export default NavBar;

import React from 'react';
import { shallow } from 'enzyme';

import NavBar from './component';

describe('<NavBar />', () => {
  test('it renders', () => {
    const wrapper = shallow(<NavBar />);
    expect(wrapper.exists()).toBe(true);
  });
});

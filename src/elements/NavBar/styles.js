import injectSheet from 'react-jss';
import colors from '../../colors';

export default injectSheet({
  navBar: {
    position: 'fixed',
    left: 0,
    width: '100%',
    height: '64px',
    backgroundColor: 'rgba(255, 255, 255, 0.9)',
    color: colors.black,
    paddingLeft: '16px',
    paddingRight: '16px',
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
  },
  top: {
    top: 0,
    borderBottom: `1px solid ${colors.primaryDark}`,
  },
  bottom: {
    bottom: 0,
    borderTop: `1px solid ${colors.primaryDark}`,
  },
  spaceBetween: {
    justifyContent: 'space-between',
  },
  spaceAround: {
    justifyContent: 'space-around',
  },
});

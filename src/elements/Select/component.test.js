import React from 'react';
import { shallow } from 'enzyme';

import Select from './component';

describe('<Select />', () => {
  test('it renders', () => {
    const wrapper = shallow(<Select />);
    expect(wrapper.exists()).toBe(true);
  });
});

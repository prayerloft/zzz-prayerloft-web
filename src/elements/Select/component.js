import React from 'react';
import PropTypes from 'prop-types';

const valueType = PropTypes.oneOfType([PropTypes.string, PropTypes.number]);

class Select extends React.PureComponent {
  static propTypes = {
    options: PropTypes.arrayOf(
      PropTypes.shape({
        label: PropTypes.string,
        value: valueType,
      }),
    ),
    value: valueType,
    onChange: PropTypes.func,
    classes: PropTypes.object,
    label: PropTypes.string,
    isError: PropTypes.bool,
  };

  static defaultProps = {
    options: [],
    value: '',
    onChange: () => null,
    label: '',
    isError: false,
  };

  onChange = event => {
    this.props.onChange(event.target.value);
  };

  getInputId() {
    return `${this.props.label}`;
  }

  getSelectClasses() {
    const { classes, isError } = this.props;

    if (!classes) return '';

    return isError ? classes.selectError : classes.select;
  }

  renderOption = ({ label, value }) => (
    <option value={value} key={value}>
      {label}
    </option>
  );

  renderOptions() {
    return this.props.options.map(this.renderOption);
  }

  render() {
    const id = this.getInputId();
    const classes = this.props.classes || {};
    return (
      <div className={classes.root}>
        <label htmlFor={id} className={classes.label}>
          {this.props.label}
        </label>
        <select
          id={id}
          value={this.props.value}
          onChange={this.onChange}
          className={this.getSelectClasses()}
        >
          {this.renderOptions()}
        </select>
      </div>
    );
  }
}

export default Select;

import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import globalStylesDecorator from '../../../stories/globalStylesDecorator';
import Select from './';

const items = [
  { label: 'hello', value: 'world' },
  { label: 'foo', value: 'bar' },
];

storiesOf('Select', module)
  .addDecorator(globalStylesDecorator)
  .add('with items', () => (
    <Select label="The label" options={items} onChange={action('onChange')} />
  ));

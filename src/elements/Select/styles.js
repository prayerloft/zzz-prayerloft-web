import colors from '../../colors';
import injectSheet from 'react-jss';

export default injectSheet({
  select: {
    fontWeight: '300',
    fontSize: '18px',
    width: '100%',
    border: 'none',
    backgroundColor: 'transparent',
    borderBottom: `2px solid ${colors.primaryDark}`,
    marginBottom: '1px',
    padding: '8px',
    outline: 'none',
    color: colors.black,
    borderRadius: 0,
    appearance: 'none',
    '&:focus': {
      borderBottom: `3px solid ${colors.black}`,
      marginBottom: 0,
    },
  },
  selectError: {
    extend: 'select',
    color: colors.error,
    borderBottomColor: colors.error,
    '&:focus': {
      borderBottom: `3px solid ${colors.error}`,
      marginBottom: 0,
    },
  },
  label: {
    fontWeight: '300',
    color: colors.primaryDark,
    fontSize: '16px',
  },
  root: {
    width: '100%',
    position: 'relative',
    '&:after': {
      position: 'absolute',
      content: '""',
      borderLeft: `2px solid ${colors.primaryDark}`,
      borderBottom: `2px solid ${colors.primaryDark}`,
      top: '34px',
      right: '10px',
      width: '8px',
      height: '8px',
      transform: 'rotate(-45deg)',
    },
  },
});

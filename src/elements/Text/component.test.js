import React from 'react';
import { shallow } from 'enzyme';

import Text from './component';

describe('<Text />', () => {
  test('it renders', () => {
    const wrapper = shallow(<Text />);
    expect(wrapper.exists()).toBe(true);
  });
});

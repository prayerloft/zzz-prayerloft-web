import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

export class Text extends PureComponent {
  static propTypes = {
    children: PropTypes.node,
    classes: PropTypes.object,
  };

  static defaultProps = {
    children: null,
  };

  render() {
    const classes = this.props.classes || {};

    const textClass = classnames(classes.text);

    return <div className={textClass}>{this.props.children}</div>;
  }
}

export default Text;

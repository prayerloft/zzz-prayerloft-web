import React from 'react';
import { storiesOf } from '@storybook/react';
import globalStylesDecorator from '../../../stories/globalStylesDecorator';

import Text from './';

storiesOf('Text', module)
  .addDecorator(globalStylesDecorator)
  .add('with text', () => <Text>Hello World</Text>);

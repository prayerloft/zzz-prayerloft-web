import injectSheet from 'react-jss';

export default injectSheet({
  text: {
    paddingTop: '8px',
    fontWeight: '300',
  },
});

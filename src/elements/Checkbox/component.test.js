import React from 'react';
import { shallow } from 'enzyme';

import Checkbox from './component';

describe('<Checkbox />', () => {
  test('it renders', () => {
    const wrapper = shallow(<Checkbox />);
    expect(wrapper.exists()).toBe(true);
  });
});

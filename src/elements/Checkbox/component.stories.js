import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { withKnobs, text, boolean } from '@storybook/addon-knobs';
import globalStylesDecorator from '../../../stories/globalStylesDecorator';

import Checkbox from './';

storiesOf('Checkbox', module)
  .addDecorator(globalStylesDecorator)
  .addDecorator(withKnobs)
  .add('with label', () => (
    <Checkbox
      onChange={action('changed')}
      value={boolean('value', true)}
      label={text('label', 'hello')}
    />
  ));

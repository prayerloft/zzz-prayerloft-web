import React from 'react';
import PropTypes from 'prop-types';
import uuid from 'uuid/v4';

import { noop } from '../../utils/defaults';

class Checkbox extends React.PureComponent {
  static propTypes = {
    classes: PropTypes.object,
    value: PropTypes.bool,
    onChange: PropTypes.func,
    label: PropTypes.node,
    isReadOnly: PropTypes.bool,
  };

  static defaultProps = {
    value: false,
    onChange: noop,
    label: null,
    isReadOnly: false,
  };

  constructor(props) {
    super(props);

    this.state = {
      id: uuid(),
    };
  }

  onChange = event => {
    if (!this.props.isReadOnly) {
      this.props.onChange(event.target.checked);
    }
  };

  render() {
    const classes = this.props.classes || {};

    return (
      <div className={classes.wrapper}>
        <input
          id={this.state.id}
          type="checkbox"
          onChange={this.onChange}
          checked={this.props.value}
          className={classes.checkbox}
        />
        <label htmlFor={this.state.id} className={classes.label}>
          {this.props.label}
        </label>
      </div>
    );
  }
}

export default Checkbox;

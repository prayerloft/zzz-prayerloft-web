import injectSheet from 'react-jss';
import colors from '../../colors';

export default injectSheet({
  wrapper: {
    position: 'relative',
  },
  checkbox: {
    appearance: 'none',
    display: 'none',
  },
  label: {
    position: 'relative',
    paddingLeft: '24px',
    height: '24px',
    display: 'flex',
    alignItems: 'center',
    color: colors.black,
    fontWeight: '300',
    fontSize: '16px',
    '&::before': {
      position: 'absolute',
      display: 'inline-block',
      top: '0',
      left: '0',
      height: '18px',
      width: '18px',
      margin: '3px',
      border: `2px solid ${colors.primaryDark}`,
      content: '""',
      transition: [
        ['background-color 200ms ease-in-out'],
        ['border 200ms ease-in-out'],
      ],
    },
    '&::after': {
      position: 'absolute',
      display: 'inline-block',
      top: '6px',
      left: '9px',
      width: '6px',
      height: '9px',
      borderTop: `2px solid ${colors.white}`,
      borderLeft: `2px solid ${colors.white}`,
      content: '""',
      transition: [['transform 200ms ease-in-out']],
    },
    '$checkbox:checked + &': {
      '&::before': {
        borderColor: colors.secondary,
        backgroundColor: colors.secondary,
      },
      '&::after': {
        transform: 'rotate(-135deg)',
      },
    },
  },
});

import injectSheet from 'react-jss';
import colors from '../../colors';

export default injectSheet({
  wrapper: {
    position: 'relative',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    padding: '32px',
    color: colors.primaryDark,
    textAlign: 'center',
  },
});

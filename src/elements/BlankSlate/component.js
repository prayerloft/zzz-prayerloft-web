import React from 'react';
import PropTypes from 'prop-types';
import Header from '../Header';

class BlankSlate extends React.PureComponent {
  static propTypes = {
    classes: PropTypes.object,
    title: PropTypes.string,
    subtitle: PropTypes.string,
  };

  static defaultProps = {
    title: '',
    subtitle: '',
  };

  render() {
    const classes = this.props.classes || {};
    const { title, subtitle } = this.props;

    return (
      <div className={classes.wrapper}>
        <Header>{title}</Header>
        <Header size="h2">{subtitle}</Header>
      </div>
    );
  }
}

export default BlankSlate;

import React from 'react';
import { shallow } from 'enzyme';

import BlankSlate from './component';

describe('<BlankSlate />', () => {
  test('it renders', () => {
    const wrapper = shallow(<BlankSlate />);
    expect(wrapper.exists()).toBe(true);
  });
});
